<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreatePublisherRequest;
use App\Http\Requests\UpdatePublisherRequest;
use App\Http\Controllers\AppBaseController;
use App\Models\Publisher;
use Illuminate\Http\Request;
use Flash;
use Response;

class PublisherController extends AppBaseController
{
    /**
     * Display a listing of the Publisher.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        /** @var Publisher $publishers */
        $publishers = Publisher::paginate(10);

        return view('publishers.index')
            ->with('publishers', $publishers);
    }

    /**
     * Show the form for creating a new Publisher.
     *
     * @return Response
     */
    public function create()
    {
        return view('publishers.create');
    }

    /**
     * Store a newly created Publisher in storage.
     *
     * @param CreatePublisherRequest $request
     *
     * @return Response
     */
    public function store(CreatePublisherRequest $request)
    {
        $input = $request->all();

        /** @var Publisher $publisher */
        $publisher = Publisher::create($input);

        Flash::success('Publisher saved successfully.');

        return redirect(route('publishers.index'));
    }

    /**
     * Display the specified Publisher.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Publisher $publisher */
        $publisher = Publisher::find($id);

        if (empty($publisher)) {
            Flash::error('Publisher not found');

            return redirect(route('publishers.index'));
        }

        return view('publishers.show')->with('publisher', $publisher);
    }

    /**
     * Show the form for editing the specified Publisher.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        /** @var Publisher $publisher */
        $publisher = Publisher::find($id);

        if (empty($publisher)) {
            Flash::error('Publisher not found');

            return redirect(route('publishers.index'));
        }

        return view('publishers.edit')->with('publisher', $publisher);
    }

    /**
     * Update the specified Publisher in storage.
     *
     * @param int $id
     * @param UpdatePublisherRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePublisherRequest $request)
    {
        /** @var Publisher $publisher */
        $publisher = Publisher::find($id);

        if (empty($publisher)) {
            Flash::error('Publisher not found');

            return redirect(route('publishers.index'));
        }

        $publisher->fill($request->all());
        $publisher->save();

        Flash::success('Publisher updated successfully.');

        return redirect(route('publishers.index'));
    }

    /**
     * Remove the specified Publisher from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Publisher $publisher */
        $publisher = Publisher::find($id);

        if (empty($publisher)) {
            Flash::error('Publisher not found');

            return redirect(route('publishers.index'));
        }

        $publisher->delete();

        Flash::success('Publisher deleted successfully.');

        return redirect(route('publishers.index'));
    }
}
