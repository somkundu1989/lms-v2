<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateAcademicYearRequest;
use App\Http\Requests\UpdateAcademicYearRequest;
use App\Http\Controllers\AppBaseController;
use App\Models\AcademicYear;
use Illuminate\Http\Request;
use Flash;
use Response;

class AcademicYearController extends AppBaseController
{
    /**
     * Display a listing of the AcademicYear.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        /** @var AcademicYear $academicYears */
        $academicYears = AcademicYear::paginate(10);

        return view('academic_years.index')
            ->with('academicYears', $academicYears);
    }

    /**
     * Show the form for creating a new AcademicYear.
     *
     * @return Response
     */
    public function create()
    {
        return view('academic_years.create');
    }

    /**
     * Store a newly created AcademicYear in storage.
     *
     * @param CreateAcademicYearRequest $request
     *
     * @return Response
     */
    public function store(CreateAcademicYearRequest $request)
    {
        $input = $request->all();

        /** @var AcademicYear $academicYear */
        $academicYear = AcademicYear::create($input);

        Flash::success('Academic Year saved successfully.');

        return redirect(route('academicYears.index'));
    }

    /**
     * Display the specified AcademicYear.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var AcademicYear $academicYear */
        $academicYear = AcademicYear::find($id);

        if (empty($academicYear)) {
            Flash::error('Academic Year not found');

            return redirect(route('academicYears.index'));
        }

        return view('academic_years.show')->with('academicYear', $academicYear);
    }

    /**
     * Show the form for editing the specified AcademicYear.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        /** @var AcademicYear $academicYear */
        $academicYear = AcademicYear::find($id);

        if (empty($academicYear)) {
            Flash::error('Academic Year not found');

            return redirect(route('academicYears.index'));
        }

        return view('academic_years.edit')->with('academicYear', $academicYear);
    }

    /**
     * Update the specified AcademicYear in storage.
     *
     * @param int $id
     * @param UpdateAcademicYearRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateAcademicYearRequest $request)
    {
        /** @var AcademicYear $academicYear */
        $academicYear = AcademicYear::find($id);

        if (empty($academicYear)) {
            Flash::error('Academic Year not found');

            return redirect(route('academicYears.index'));
        }

        $academicYear->fill($request->all());
        $academicYear->save();

        Flash::success('Academic Year updated successfully.');

        return redirect(route('academicYears.index'));
    }

    /**
     * Remove the specified AcademicYear from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var AcademicYear $academicYear */
        $academicYear = AcademicYear::find($id);

        if (empty($academicYear)) {
            Flash::error('Academic Year not found');

            return redirect(route('academicYears.index'));
        }

        $academicYear->delete();

        Flash::success('Academic Year deleted successfully.');

        return redirect(route('academicYears.index'));
    }
}
