<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateissueRequest;
use App\Http\Requests\UpdateissueRequest;
use App\Http\Controllers\AppBaseController;
use App\Models\issue;
use Illuminate\Http\Request;
use Flash;
use Response;

class issueController extends AppBaseController
{
    /**
     * Display a listing of the issue.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        /** @var issue $issues */
        $issues = issue::paginate(10);

        return view('issues.index')
            ->with('issues', $issues);
    }

    /**
     * Show the form for creating a new issue.
     *
     * @return Response
     */
    public function create()
    {
        return view('issues.create');
    }

    /**
     * Store a newly created issue in storage.
     *
     * @param CreateissueRequest $request
     *
     * @return Response
     */
    public function store(CreateissueRequest $request)
    {
        $input = $request->all();

        /** @var issue $issue */
        $issue = issue::create($input);

        Flash::success('Issue saved successfully.');

        return redirect(route('issues.index'));
    }

    /**
     * Display the specified issue.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var issue $issue */
        $issue = issue::find($id);

        if (empty($issue)) {
            Flash::error('Issue not found');

            return redirect(route('issues.index'));
        }

        return view('issues.show')->with('issue', $issue);
    }

    /**
     * Show the form for editing the specified issue.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        /** @var issue $issue */
        $issue = issue::find($id);

        if (empty($issue)) {
            Flash::error('Issue not found');

            return redirect(route('issues.index'));
        }

        return view('issues.edit')->with('issue', $issue);
    }

    /**
     * Update the specified issue in storage.
     *
     * @param int $id
     * @param UpdateissueRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateissueRequest $request)
    {
        /** @var issue $issue */
        $issue = issue::find($id);

        if (empty($issue)) {
            Flash::error('Issue not found');

            return redirect(route('issues.index'));
        }

        $issue->fill($request->all());
        $issue->save();

        Flash::success('Issue updated successfully.');

        return redirect(route('issues.index'));
    }

    /**
     * Remove the specified issue from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var issue $issue */
        $issue = issue::find($id);

        if (empty($issue)) {
            Flash::error('Issue not found');

            return redirect(route('issues.index'));
        }

        $issue->delete();

        Flash::success('Issue deleted successfully.');

        return redirect(route('issues.index'));
    }
}
