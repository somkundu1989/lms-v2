<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateIssueCategoryRequest;
use App\Http\Requests\UpdateIssueCategoryRequest;
use App\Http\Controllers\AppBaseController;
use App\Models\IssueCategory;
use Illuminate\Http\Request;
use Flash;
use Response;

class IssueCategoryController extends AppBaseController
{
    /**
     * Display a listing of the IssueCategory.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        /** @var IssueCategory $issueCategories */
        $issueCategories = IssueCategory::paginate(10);

        return view('issue_categories.index')
            ->with('issueCategories', $issueCategories);
    }

    /**
     * Show the form for creating a new IssueCategory.
     *
     * @return Response
     */
    public function create()
    {
        return view('issue_categories.create');
    }

    /**
     * Store a newly created IssueCategory in storage.
     *
     * @param CreateIssueCategoryRequest $request
     *
     * @return Response
     */
    public function store(CreateIssueCategoryRequest $request)
    {
        $input = $request->all();

        /** @var IssueCategory $issueCategory */
        $issueCategory = IssueCategory::create($input);

        Flash::success('Issue Category saved successfully.');

        return redirect(route('issueCategories.index'));
    }

    /**
     * Display the specified IssueCategory.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var IssueCategory $issueCategory */
        $issueCategory = IssueCategory::find($id);

        if (empty($issueCategory)) {
            Flash::error('Issue Category not found');

            return redirect(route('issueCategories.index'));
        }

        return view('issue_categories.show')->with('issueCategory', $issueCategory);
    }

    /**
     * Show the form for editing the specified IssueCategory.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        /** @var IssueCategory $issueCategory */
        $issueCategory = IssueCategory::find($id);

        if (empty($issueCategory)) {
            Flash::error('Issue Category not found');

            return redirect(route('issueCategories.index'));
        }

        return view('issue_categories.edit')->with('issueCategory', $issueCategory);
    }

    /**
     * Update the specified IssueCategory in storage.
     *
     * @param int $id
     * @param UpdateIssueCategoryRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateIssueCategoryRequest $request)
    {
        /** @var IssueCategory $issueCategory */
        $issueCategory = IssueCategory::find($id);

        if (empty($issueCategory)) {
            Flash::error('Issue Category not found');

            return redirect(route('issueCategories.index'));
        }

        $issueCategory->fill($request->all());
        $issueCategory->save();

        Flash::success('Issue Category updated successfully.');

        return redirect(route('issueCategories.index'));
    }

    /**
     * Remove the specified IssueCategory from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var IssueCategory $issueCategory */
        $issueCategory = IssueCategory::find($id);

        if (empty($issueCategory)) {
            Flash::error('Issue Category not found');

            return redirect(route('issueCategories.index'));
        }

        $issueCategory->delete();

        Flash::success('Issue Category deleted successfully.');

        return redirect(route('issueCategories.index'));
    }
}
