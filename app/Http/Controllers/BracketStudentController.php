<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateBracketStudentRequest;
use App\Http\Requests\UpdateBracketStudentRequest;
use App\Http\Controllers\AppBaseController;
use App\Models\BracketStudent;
use Illuminate\Http\Request;
use Flash;
use Response;

class BracketStudentController extends AppBaseController
{
    /**
     * Display a listing of the BracketStudent.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        /** @var BracketStudent $bracketStudents */
        $bracketStudents = BracketStudent::paginate(10);

        return view('bracket_students.index')
            ->with('bracketStudents', $bracketStudents);
    }

    /**
     * Show the form for creating a new BracketStudent.
     *
     * @return Response
     */
    public function create()
    {
        return view('bracket_students.create');
    }

    /**
     * Store a newly created BracketStudent in storage.
     *
     * @param CreateBracketStudentRequest $request
     *
     * @return Response
     */
    public function store(CreateBracketStudentRequest $request)
    {
        $input = $request->all();

        /** @var BracketStudent $bracketStudent */
        $bracketStudent = BracketStudent::create($input);

        Flash::success('Bracket Student saved successfully.');

        return redirect(route('bracketStudents.index'));
    }

    /**
     * Display the specified BracketStudent.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var BracketStudent $bracketStudent */
        $bracketStudent = BracketStudent::find($id);

        if (empty($bracketStudent)) {
            Flash::error('Bracket Student not found');

            return redirect(route('bracketStudents.index'));
        }

        return view('bracket_students.show')->with('bracketStudent', $bracketStudent);
    }

    /**
     * Show the form for editing the specified BracketStudent.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        /** @var BracketStudent $bracketStudent */
        $bracketStudent = BracketStudent::find($id);

        if (empty($bracketStudent)) {
            Flash::error('Bracket Student not found');

            return redirect(route('bracketStudents.index'));
        }

        return view('bracket_students.edit')->with('bracketStudent', $bracketStudent);
    }

    /**
     * Update the specified BracketStudent in storage.
     *
     * @param int $id
     * @param UpdateBracketStudentRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateBracketStudentRequest $request)
    {
        /** @var BracketStudent $bracketStudent */
        $bracketStudent = BracketStudent::find($id);

        if (empty($bracketStudent)) {
            Flash::error('Bracket Student not found');

            return redirect(route('bracketStudents.index'));
        }

        $bracketStudent->fill($request->all());
        $bracketStudent->save();

        Flash::success('Bracket Student updated successfully.');

        return redirect(route('bracketStudents.index'));
    }

    /**
     * Remove the specified BracketStudent from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var BracketStudent $bracketStudent */
        $bracketStudent = BracketStudent::find($id);

        if (empty($bracketStudent)) {
            Flash::error('Bracket Student not found');

            return redirect(route('bracketStudents.index'));
        }

        $bracketStudent->delete();

        Flash::success('Bracket Student deleted successfully.');

        return redirect(route('bracketStudents.index'));
    }
}
