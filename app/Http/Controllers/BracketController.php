<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateBracketRequest;
use App\Http\Requests\UpdateBracketRequest;
use App\Http\Controllers\AppBaseController;
use App\Models\Bracket;
use Illuminate\Http\Request;
use Flash;
use Response;

class BracketController extends AppBaseController
{
    /**
     * Display a listing of the Bracket.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        /** @var Bracket $brackets */
        $brackets = Bracket::paginate(10);

        return view('brackets.index')
            ->with('brackets', $brackets);
    }

    /**
     * Show the form for creating a new Bracket.
     *
     * @return Response
     */
    public function create()
    {
        return view('brackets.create');
    }

    /**
     * Store a newly created Bracket in storage.
     *
     * @param CreateBracketRequest $request
     *
     * @return Response
     */
    public function store(CreateBracketRequest $request)
    {
        $input = $request->all();

        /** @var Bracket $bracket */
        $bracket = Bracket::create($input);

        Flash::success('Bracket saved successfully.');

        return redirect(route('brackets.index'));
    }

    /**
     * Display the specified Bracket.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Bracket $bracket */
        $bracket = Bracket::find($id);

        if (empty($bracket)) {
            Flash::error('Bracket not found');

            return redirect(route('brackets.index'));
        }

        return view('brackets.show')->with('bracket', $bracket);
    }

    /**
     * Show the form for editing the specified Bracket.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        /** @var Bracket $bracket */
        $bracket = Bracket::find($id);

        if (empty($bracket)) {
            Flash::error('Bracket not found');

            return redirect(route('brackets.index'));
        }

        return view('brackets.edit')->with('bracket', $bracket);
    }

    /**
     * Update the specified Bracket in storage.
     *
     * @param int $id
     * @param UpdateBracketRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateBracketRequest $request)
    {
        /** @var Bracket $bracket */
        $bracket = Bracket::find($id);

        if (empty($bracket)) {
            Flash::error('Bracket not found');

            return redirect(route('brackets.index'));
        }

        $bracket->fill($request->all());
        $bracket->save();

        Flash::success('Bracket updated successfully.');

        return redirect(route('brackets.index'));
    }

    /**
     * Remove the specified Bracket from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Bracket $bracket */
        $bracket = Bracket::find($id);

        if (empty($bracket)) {
            Flash::error('Bracket not found');

            return redirect(route('brackets.index'));
        }

        $bracket->delete();

        Flash::success('Bracket deleted successfully.');

        return redirect(route('brackets.index'));
    }
}
