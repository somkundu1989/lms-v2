<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateAssesmentRequest;
use App\Http\Requests\UpdateAssesmentRequest;
use App\Http\Controllers\AppBaseController;
use App\Models\Assesment;
use Illuminate\Http\Request;
use Flash;
use Response;

class AssesmentController extends AppBaseController
{
    /**
     * Display a listing of the Assesment.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        /** @var Assesment $assesments */
        $assesments = Assesment::paginate(10);

        return view('assesments.index')
            ->with('assesments', $assesments);
    }

    /**
     * Show the form for creating a new Assesment.
     *
     * @return Response
     */
    public function create()
    {
        return view('assesments.create');
    }

    /**
     * Store a newly created Assesment in storage.
     *
     * @param CreateAssesmentRequest $request
     *
     * @return Response
     */
    public function store(CreateAssesmentRequest $request)
    {
        $input = $request->all();

        /** @var Assesment $assesment */
        $assesment = Assesment::create($input);

        Flash::success('Assesment saved successfully.');

        return redirect(route('assesments.index'));
    }

    /**
     * Display the specified Assesment.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Assesment $assesment */
        $assesment = Assesment::find($id);

        if (empty($assesment)) {
            Flash::error('Assesment not found');

            return redirect(route('assesments.index'));
        }

        return view('assesments.show')->with('assesment', $assesment);
    }

    /**
     * Show the form for editing the specified Assesment.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        /** @var Assesment $assesment */
        $assesment = Assesment::find($id);

        if (empty($assesment)) {
            Flash::error('Assesment not found');

            return redirect(route('assesments.index'));
        }

        return view('assesments.edit')->with('assesment', $assesment);
    }

    /**
     * Update the specified Assesment in storage.
     *
     * @param int $id
     * @param UpdateAssesmentRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateAssesmentRequest $request)
    {
        /** @var Assesment $assesment */
        $assesment = Assesment::find($id);

        if (empty($assesment)) {
            Flash::error('Assesment not found');

            return redirect(route('assesments.index'));
        }

        $assesment->fill($request->all());
        $assesment->save();

        Flash::success('Assesment updated successfully.');

        return redirect(route('assesments.index'));
    }

    /**
     * Remove the specified Assesment from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Assesment $assesment */
        $assesment = Assesment::find($id);

        if (empty($assesment)) {
            Flash::error('Assesment not found');

            return redirect(route('assesments.index'));
        }

        $assesment->delete();

        Flash::success('Assesment deleted successfully.');

        return redirect(route('assesments.index'));
    }
}
