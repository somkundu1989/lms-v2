<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateBatcheRequest;
use App\Http\Requests\UpdateBatcheRequest;
use App\Http\Controllers\AppBaseController;
use App\Models\Batche;
use Illuminate\Http\Request;
use Flash;
use Response;

class BatcheController extends AppBaseController
{
    /**
     * Display a listing of the Batche.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        /** @var Batche $batches */
        $batches = Batche::paginate(10);

        return view('batches.index')
            ->with('batches', $batches);
    }

    /**
     * Show the form for creating a new Batche.
     *
     * @return Response
     */
    public function create()
    {
        return view('batches.create');
    }

    /**
     * Store a newly created Batche in storage.
     *
     * @param CreateBatcheRequest $request
     *
     * @return Response
     */
    public function store(CreateBatcheRequest $request)
    {
        $input = $request->all();

        /** @var Batche $batche */
        $input['subjects'] = json_encode($input['subjects']);
        $batche = Batche::create($input);

        Flash::success('Batche saved successfully.');

        return redirect(route('batches.index'));
    }

    /**
     * Display the specified Batche.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Batche $batche */
        $batche = Batche::find($id);

        if (empty($batche)) {
            Flash::error('Batche not found');

            return redirect(route('batches.index'));
        }

        return view('batches.show')->with('batche', $batche);
    }

    /**
     * Show the form for editing the specified Batche.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        /** @var Batche $batche */
        $batche = Batche::find($id);

        if (empty($batche)) {
            Flash::error('Batche not found');

            return redirect(route('batches.index'));
        }

        return view('batches.edit')->with('batche', $batche);
    }

    /**
     * Update the specified Batche in storage.
     *
     * @param int $id
     * @param UpdateBatcheRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateBatcheRequest $request)
    {
        /** @var Batche $batche */
        $batche = Batche::find($id);

        if (empty($batche)) {
            Flash::error('Batche not found');

            return redirect(route('batches.index'));
        }
        $input = $request->all();

        $input['subjects'] = json_encode($input['subjects']);
        $batche->fill($input);
        $batche->save();

        Flash::success('Batche updated successfully.');

        return redirect(route('batches.index'));
    }

    /**
     * Remove the specified Batche from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Batche $batche */
        $batche = Batche::find($id);

        if (empty($batche)) {
            Flash::error('Batche not found');

            return redirect(route('batches.index'));
        }

        $batche->delete();

        Flash::success('Batche deleted successfully.');

        return redirect(route('batches.index'));
    }
}
