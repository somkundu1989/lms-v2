<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateAssesmentStudentRequest;
use App\Http\Requests\UpdateAssesmentStudentRequest;
use App\Http\Controllers\AppBaseController;
use App\Models\AssesmentStudent;
use Illuminate\Http\Request;
use Flash;
use Response;

class AssesmentStudentController extends AppBaseController
{
    /**
     * Display a listing of the AssesmentStudent.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        /** @var AssesmentStudent $assesmentStudents */
        $assesmentStudents = AssesmentStudent::paginate(10);

        return view('assesment_students.index')
            ->with('assesmentStudents', $assesmentStudents);
    }

    /**
     * Show the form for creating a new AssesmentStudent.
     *
     * @return Response
     */
    public function create()
    {
        return view('assesment_students.create');
    }

    /**
     * Store a newly created AssesmentStudent in storage.
     *
     * @param CreateAssesmentStudentRequest $request
     *
     * @return Response
     */
    public function store(CreateAssesmentStudentRequest $request)
    {
        $input = $request->all();

        /** @var AssesmentStudent $assesmentStudent */
        $assesmentStudent = AssesmentStudent::create($input);

        Flash::success('Assesment Student saved successfully.');

        return redirect(route('assesmentStudents.index'));
    }

    /**
     * Display the specified AssesmentStudent.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var AssesmentStudent $assesmentStudent */
        $assesmentStudent = AssesmentStudent::find($id);

        if (empty($assesmentStudent)) {
            Flash::error('Assesment Student not found');

            return redirect(route('assesmentStudents.index'));
        }

        return view('assesment_students.show')->with('assesmentStudent', $assesmentStudent);
    }

    /**
     * Show the form for editing the specified AssesmentStudent.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        /** @var AssesmentStudent $assesmentStudent */
        $assesmentStudent = AssesmentStudent::find($id);

        if (empty($assesmentStudent)) {
            Flash::error('Assesment Student not found');

            return redirect(route('assesmentStudents.index'));
        }

        return view('assesment_students.edit')->with('assesmentStudent', $assesmentStudent);
    }

    /**
     * Update the specified AssesmentStudent in storage.
     *
     * @param int $id
     * @param UpdateAssesmentStudentRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateAssesmentStudentRequest $request)
    {
        /** @var AssesmentStudent $assesmentStudent */
        $assesmentStudent = AssesmentStudent::find($id);

        if (empty($assesmentStudent)) {
            Flash::error('Assesment Student not found');

            return redirect(route('assesmentStudents.index'));
        }

        $assesmentStudent->fill($request->all());
        $assesmentStudent->save();

        Flash::success('Assesment Student updated successfully.');

        return redirect(route('assesmentStudents.index'));
    }

    /**
     * Remove the specified AssesmentStudent from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var AssesmentStudent $assesmentStudent */
        $assesmentStudent = AssesmentStudent::find($id);

        if (empty($assesmentStudent)) {
            Flash::error('Assesment Student not found');

            return redirect(route('assesmentStudents.index'));
        }

        $assesmentStudent->delete();

        Flash::success('Assesment Student deleted successfully.');

        return redirect(route('assesmentStudents.index'));
    }
}
