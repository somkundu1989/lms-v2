<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateGenreRequest;
use App\Http\Requests\UpdateGenreRequest;
use App\Http\Controllers\AppBaseController;
use App\Models\Genre;
use Illuminate\Http\Request;
use Flash;
use Response;

class GenreController extends AppBaseController
{
    /**
     * Display a listing of the Genre.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        /** @var Genre $genres */
        $genres = Genre::paginate(10);

        return view('genres.index')
            ->with('genres', $genres);
    }

    /**
     * Show the form for creating a new Genre.
     *
     * @return Response
     */
    public function create()
    {
        return view('genres.create');
    }

    /**
     * Store a newly created Genre in storage.
     *
     * @param CreateGenreRequest $request
     *
     * @return Response
     */
    public function store(CreateGenreRequest $request)
    {
        $input = $request->all();

        /** @var Genre $genre */
        $genre = Genre::create($input);

        Flash::success('Genre saved successfully.');

        return redirect(route('genres.index'));
    }

    /**
     * Display the specified Genre.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Genre $genre */
        $genre = Genre::find($id);

        if (empty($genre)) {
            Flash::error('Genre not found');

            return redirect(route('genres.index'));
        }

        return view('genres.show')->with('genre', $genre);
    }

    /**
     * Show the form for editing the specified Genre.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        /** @var Genre $genre */
        $genre = Genre::find($id);

        if (empty($genre)) {
            Flash::error('Genre not found');

            return redirect(route('genres.index'));
        }

        return view('genres.edit')->with('genre', $genre);
    }

    /**
     * Update the specified Genre in storage.
     *
     * @param int $id
     * @param UpdateGenreRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateGenreRequest $request)
    {
        /** @var Genre $genre */
        $genre = Genre::find($id);

        if (empty($genre)) {
            Flash::error('Genre not found');

            return redirect(route('genres.index'));
        }

        $genre->fill($request->all());
        $genre->save();

        Flash::success('Genre updated successfully.');

        return redirect(route('genres.index'));
    }

    /**
     * Remove the specified Genre from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Genre $genre */
        $genre = Genre::find($id);

        if (empty($genre)) {
            Flash::error('Genre not found');

            return redirect(route('genres.index'));
        }

        $genre->delete();

        Flash::success('Genre deleted successfully.');

        return redirect(route('genres.index'));
    }
}
