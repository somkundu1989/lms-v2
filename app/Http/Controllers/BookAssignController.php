<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateBookAssignRequest;
use App\Http\Requests\UpdateBookAssignRequest;
use App\Http\Controllers\AppBaseController;
use App\Models\BookAssign;
use Illuminate\Http\Request;
use Flash;
use Response;

class BookAssignController extends AppBaseController
{
    /**
     * Display a listing of the BookAssign.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        /** @var BookAssign $bookAssigns */
        $bookAssigns = BookAssign::paginate(10);

        return view('book_assigns.index')
            ->with('bookAssigns', $bookAssigns);
    }

    /**
     * Show the form for creating a new BookAssign.
     *
     * @return Response
     */
    public function create()
    {
        return view('book_assigns.create');
    }

    /**
     * Store a newly created BookAssign in storage.
     *
     * @param CreateBookAssignRequest $request
     *
     * @return Response
     */
    public function store(CreateBookAssignRequest $request)
    {
        $input = $request->all();

        /** @var BookAssign $bookAssign */
        $bookAssign = BookAssign::create($input);

        Flash::success('Book Assign saved successfully.');

        return redirect(route('bookAssigns.index'));
    }

    /**
     * Display the specified BookAssign.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var BookAssign $bookAssign */
        $bookAssign = BookAssign::find($id);

        if (empty($bookAssign)) {
            Flash::error('Book Assign not found');

            return redirect(route('bookAssigns.index'));
        }

        return view('book_assigns.show')->with('bookAssign', $bookAssign);
    }

    /**
     * Show the form for editing the specified BookAssign.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        /** @var BookAssign $bookAssign */
        $bookAssign = BookAssign::find($id);

        if (empty($bookAssign)) {
            Flash::error('Book Assign not found');

            return redirect(route('bookAssigns.index'));
        }

        return view('book_assigns.edit')->with('bookAssign', $bookAssign);
    }

    /**
     * Update the specified BookAssign in storage.
     *
     * @param int $id
     * @param UpdateBookAssignRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateBookAssignRequest $request)
    {
        /** @var BookAssign $bookAssign */
        $bookAssign = BookAssign::find($id);

        if (empty($bookAssign)) {
            Flash::error('Book Assign not found');

            return redirect(route('bookAssigns.index'));
        }

        $bookAssign->fill($request->all());
        $bookAssign->save();

        Flash::success('Book Assign updated successfully.');

        return redirect(route('bookAssigns.index'));
    }

    /**
     * Remove the specified BookAssign from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var BookAssign $bookAssign */
        $bookAssign = BookAssign::find($id);

        if (empty($bookAssign)) {
            Flash::error('Book Assign not found');

            return redirect(route('bookAssigns.index'));
        }

        $bookAssign->delete();

        Flash::success('Book Assign deleted successfully.');

        return redirect(route('bookAssigns.index'));
    }
}
