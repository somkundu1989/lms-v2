<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateInstituteRequest;
use App\Http\Requests\UpdateInstituteRequest;
use App\Http\Controllers\AppBaseController;
use App\Models\Institute;
use Illuminate\Http\Request;
use Flash;
use Response;

class InstituteController extends AppBaseController
{
    /**
     * Display a listing of the Institute.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        /** @var Institute $institutes */
        $institutes = Institute::paginate(10);

        return view('institutes.index')
            ->with('institutes', $institutes);
    }

    /**
     * Show the form for creating a new Institute.
     *
     * @return Response
     */
    public function create()
    {
        return view('institutes.create');
    }

    /**
     * Store a newly created Institute in storage.
     *
     * @param CreateInstituteRequest $request
     *
     * @return Response
     */
    public function store(CreateInstituteRequest $request)
    {
        $input = $request->all();

        /** @var Institute $institute */
        $institute = Institute::create($input);

        Flash::success('Institute saved successfully.');

        return redirect(route('institutes.index'));
    }

    /**
     * Display the specified Institute.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Institute $institute */
        $institute = Institute::find($id);

        if (empty($institute)) {
            Flash::error('Institute not found');

            return redirect(route('institutes.index'));
        }

        return view('institutes.show')->with('institute', $institute);
    }

    /**
     * Show the form for editing the specified Institute.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        /** @var Institute $institute */
        $institute = Institute::find($id);

        if (empty($institute)) {
            Flash::error('Institute not found');

            return redirect(route('institutes.index'));
        }

        return view('institutes.edit')->with('institute', $institute);
    }

    /**
     * Update the specified Institute in storage.
     *
     * @param int $id
     * @param UpdateInstituteRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateInstituteRequest $request)
    {
        /** @var Institute $institute */
        $institute = Institute::find($id);

        if (empty($institute)) {
            Flash::error('Institute not found');

            return redirect(route('institutes.index'));
        }

        $institute->fill($request->all());
        $institute->save();

        Flash::success('Institute updated successfully.');

        return redirect(route('institutes.index'));
    }

    /**
     * Remove the specified Institute from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Institute $institute */
        $institute = Institute::find($id);

        if (empty($institute)) {
            Flash::error('Institute not found');

            return redirect(route('institutes.index'));
        }

        $institute->delete();

        Flash::success('Institute deleted successfully.');

        return redirect(route('institutes.index'));
    }
}
