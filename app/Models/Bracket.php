<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;


/**
 * Class Bracket
 * @package App\Models
 * @version December 18, 2021, 10:37 am UTC
 *
 * @property \App\Models\MasterAcademicYear $academicYear
 * @property \App\Models\Batch $batch
 * @property \App\Models\MasterCourse $course
 * @property App\Models\Institute $institute
 * @property \App\Models\MasterSubject $subject
 * @property \App\Models\User $teacher
 * @property \Illuminate\Database\Eloquent\Collection $eduBracketStudents
 * @property \Illuminate\Database\Eloquent\Collection $issues
 * @property integer $institute_id
 * @property integer $academic_year_id
 * @property integer $course_id
 * @property integer $batch_id
 * @property string|\Carbon\Carbon $started_at
 * @property string|\Carbon\Carbon $ended_at
 * @property integer $teacher_id
 * @property integer $subject_id
 * @property string $title
 * @property string $students
 * @property string $meeting_link
 */
class Bracket extends Model
{
    use SoftDeletes;


    public $table = 'edu_brackets';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'institute_id',
        'academic_year_id',
        'course_id',
        'batch_id',
        'started_at',
        'ended_at',
        'teacher_id',
        'subject_id',
        'title',
        'students',
        'meeting_link'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'institute_id' => 'integer',
        'academic_year_id' => 'integer',
        'course_id' => 'integer',
        'batch_id' => 'integer',
        'started_at' => 'datetime',
        'ended_at' => 'datetime',
        'teacher_id' => 'integer',
        'subject_id' => 'integer',
        'title' => 'string',
        'students' => 'string',
        'meeting_link' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'institute_id' => 'required',
        'academic_year_id' => 'required',
        'course_id' => 'required',
        'batch_id' => 'required',
        'started_at' => 'required',
        'ended_at' => 'required',
        'teacher_id' => 'required',
        'subject_id' => 'required',
        'title' => 'required|string|max:255',
        'students' => 'required|string',
        'meeting_link' => 'required|string|max:255',
        'created_at' => 'nullable',
        'updated_at' => 'nullable',
        'deleted_at' => 'nullable'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function academicYear()
    {
        return $this->belongsTo(\App\Models\AcademicYear::class, 'academic_year_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function batch()
    {
        return $this->belongsTo(\App\Models\Batch::class, 'batch_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function course()
    {
        return $this->belongsTo(\App\Models\Course::class, 'course_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function institute()
    {
        return $this->belongsTo(\App\Models\Institute::class, 'institute_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function subject()
    {
        return $this->belongsTo(\App\Models\Subject::class, 'subject_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function teacher()
    {
        return $this->belongsTo(\App\Models\User::class, 'teacher_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function eduBracketStudents()
    {
        return $this->hasMany(\App\Models\BracketStudent::class, 'bracket_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function issues()
    {
        return $this->hasMany(\App\Models\Issue::class, 'bracket_id');
    }
}
