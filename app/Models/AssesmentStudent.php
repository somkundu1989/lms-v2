<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;


/**
 * Class AssesmentStudent
 * @package App\Models
 * @version December 18, 2021, 10:35 am UTC
 *
 * @property \App\Models\MasterAcademicYear $academicYear
 * @property \App\Models\Assesment $assesment
 * @property \App\Models\Batch $batch
 * @property \App\Models\MasterCourse $course
 * @property App\Models\Institute $institute
 * @property \App\Models\User $student
 * @property \App\Models\MasterSubject $subject
 * @property integer $institute_id
 * @property integer $academic_year_id
 * @property integer $course_id
 * @property integer $batch_id
 * @property integer $subject_id
 * @property integer $assesment_id
 * @property integer $student_id
 * @property string $answer_stock
 * @property boolean $marks_obtained
 * @property string $file_pdf_result
 */
class AssesmentStudent extends Model
{
    use SoftDeletes;


    public $table = 'assesment_students';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'institute_id',
        'academic_year_id',
        'course_id',
        'batch_id',
        'subject_id',
        'assesment_id',
        'student_id',
        'answer_stock',
        'marks_obtained',
        'file_pdf_result'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'institute_id' => 'integer',
        'academic_year_id' => 'integer',
        'course_id' => 'integer',
        'batch_id' => 'integer',
        'subject_id' => 'integer',
        'assesment_id' => 'integer',
        'student_id' => 'integer',
        'answer_stock' => 'string',
        'marks_obtained' => 'boolean',
        'file_pdf_result' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'institute_id' => 'required',
        'academic_year_id' => 'required',
        'course_id' => 'required',
        'batch_id' => 'required',
        'subject_id' => 'required',
        'assesment_id' => 'required',
        'student_id' => 'required',
        'answer_stock' => 'required|string',
        'marks_obtained' => 'nullable|boolean',
        'file_pdf_result' => 'nullable|string|max:255',
        'created_at' => 'nullable',
        'updated_at' => 'nullable',
        'deleted_at' => 'nullable'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function academicYear()
    {
        return $this->belongsTo(\App\Models\AcademicYear::class, 'academic_year_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function assesment()
    {
        return $this->belongsTo(\App\Models\Assesment::class, 'assesment_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function batch()
    {
        return $this->belongsTo(\App\Models\Batch::class, 'batch_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function course()
    {
        return $this->belongsTo(\App\Models\Course::class, 'course_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function institute()
    {
        return $this->belongsTo(\App\Models\Institute::class, 'institute_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function student()
    {
        return $this->belongsTo(\App\Models\User::class, 'student_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function subject()
    {
        return $this->belongsTo(\App\Models\Subject::class, 'subject_id');
    }
}
