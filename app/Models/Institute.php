<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;


/**
 * Class Institute
 * @package App\Models
 * @version December 18, 2021, 2:21 pm UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection $assesmentStudents
 * @property \Illuminate\Database\Eloquent\Collection $assesments
 * @property \Illuminate\Database\Eloquent\Collection $eduBatches
 * @property \Illuminate\Database\Eloquent\Collection $eduBracketStudents
 * @property \Illuminate\Database\Eloquent\Collection $eduBrackets
 * @property \Illuminate\Database\Eloquent\Collection $issues
 * @property \Illuminate\Database\Eloquent\Collection $masterAcademicYears
 * @property \Illuminate\Database\Eloquent\Collection $masterCourses
 * @property \Illuminate\Database\Eloquent\Collection $masterSubjects
 * @property \Illuminate\Database\Eloquent\Collection $orgRoles
 * @property \Illuminate\Database\Eloquent\Collection $payFees
 * @property \Illuminate\Database\Eloquent\Collection $payTransactions
 * @property string $logo
 * @property string $title
 * @property string $establishmint_date
 */
class Institute extends Model
{
    use SoftDeletes;


    public $table = 'org_institutes';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'logo',
        'title',
        'establishmint_date'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'logo' => 'string',
        'title' => 'string',
        'establishmint_date' => 'date'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'logo' => 'required|string|max:255',
        'title' => 'required|string|max:255',
        'establishmint_date' => 'required',
        'created_at' => 'nullable',
        'updated_at' => 'nullable',
        'deleted_at' => 'nullable'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function assesmentStudents()
    {
        return $this->hasMany(\App\Models\AssesmentStudent::class, 'institute_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function assesments()
    {
        return $this->hasMany(\App\Models\Assesment::class, 'institute_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function eduBatches()
    {
        return $this->hasMany(\App\Models\Batch::class, 'institute_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function eduBracketStudents()
    {
        return $this->hasMany(\App\Models\BracketStudent::class, 'institute_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function eduBrackets()
    {
        return $this->hasMany(\App\Models\Bracket::class, 'institute_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function issues()
    {
        return $this->hasMany(\App\Models\Issue::class, 'institute_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function masterAcademicYears()
    {
        return $this->hasMany(\App\Models\AcademicYear::class, 'institute_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function masterCourses()
    {
        return $this->hasMany(\App\Models\Course::class, 'institute_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function masterSubjects()
    {
        return $this->hasMany(\App\Models\Subject::class, 'institute_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function orgRoles()
    {
        return $this->hasMany(\App\Models\Role::class, 'institute_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function payFees()
    {
        return $this->hasMany(\App\Models\PayFee::class, 'institute_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function payTransactions()
    {
        return $this->hasMany(\App\Models\PayTransaction::class, 'institute_id');
    }
}
