<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;


/**
 * Class Transaction
 * @package App\Models
 * @version December 18, 2021, 10:38 am UTC
 *
 * @property \App\Models\MasterAcademicYear $academicYear
 * @property \App\Models\Batch $batch
 * @property \App\Models\LibBookAssign $bookAssign
 * @property \App\Models\LibBook $book
 * @property \App\Models\MasterCourse $course
 * @property \App\Models\PayFee $fee
 * @property App\Models\Institute $institute
 * @property \App\Models\User $user
 * @property integer $institute_id
 * @property integer $academic_year_id
 * @property integer $fee_id
 * @property integer $user_id
 * @property integer $course_id
 * @property integer $batch_id
 * @property integer $book_id
 * @property integer $book_assign_id
 * @property integer $amount
 * @property string $from_date
 * @property string $to_date
 */
class Transaction extends Model
{
    use SoftDeletes;


    public $table = 'pay_transactions';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'institute_id',
        'academic_year_id',
        'fee_id',
        'user_id',
        'course_id',
        'batch_id',
        'book_id',
        'book_assign_id',
        'amount',
        'from_date',
        'to_date'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'institute_id' => 'integer',
        'academic_year_id' => 'integer',
        'fee_id' => 'integer',
        'user_id' => 'integer',
        'course_id' => 'integer',
        'batch_id' => 'integer',
        'book_id' => 'integer',
        'book_assign_id' => 'integer',
        'amount' => 'integer',
        'from_date' => 'date',
        'to_date' => 'date'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'institute_id' => 'required',
        'academic_year_id' => 'required',
        'fee_id' => 'required',
        'user_id' => 'required',
        'course_id' => 'nullable',
        'batch_id' => 'nullable',
        'book_id' => 'nullable',
        'book_assign_id' => 'nullable',
        'amount' => 'required|integer',
        'from_date' => 'nullable',
        'to_date' => 'nullable',
        'created_at' => 'nullable',
        'updated_at' => 'nullable',
        'deleted_at' => 'nullable'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function academicYear()
    {
        return $this->belongsTo(\App\Models\AcademicYear::class, 'academic_year_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function batch()
    {
        return $this->belongsTo(\App\Models\Batch::class, 'batch_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function bookAssign()
    {
        return $this->belongsTo(\App\Models\BookAssign::class, 'book_assign_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function book()
    {
        return $this->belongsTo(\App\Models\Book::class, 'book_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function course()
    {
        return $this->belongsTo(\App\Models\Course::class, 'course_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function fee()
    {
        return $this->belongsTo(\App\Models\PayFee::class, 'fee_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function institute()
    {
        return $this->belongsTo(\App\Models\Institute::class, 'institute_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function user()
    {
        return $this->belongsTo(\App\Models\User::class, 'user_id');
    }
}
