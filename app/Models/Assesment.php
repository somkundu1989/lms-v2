<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;


/**
 * Class Assesment
 * @package App\Models
 * @version December 18, 2021, 10:37 am UTC
 *
 * @property \App\Models\MasterAcademicYear $academicYear
 * @property \App\Models\MasterSubject $subject
 * @property \App\Models\Batch $batch
 * @property App\Models\Institute $institute
 * @property \App\Models\MasterCourse $course
 * @property \Illuminate\Database\Eloquent\Collection $assesmentStudents
 * @property integer $institute_id
 * @property integer $academic_year_id
 * @property integer $course_id
 * @property integer $batch_id
 * @property integer $subject_id
 * @property string|\Carbon\Carbon $started_at
 * @property string|\Carbon\Carbon $ended_at
 * @property string $title
 * @property string $students
 * @property string $question_stock
 * @property string $answer_stock
 * @property boolean $total_marks
 */
class Assesment extends Model
{
    use SoftDeletes;


    public $table = 'assesments';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'institute_id',
        'academic_year_id',
        'course_id',
        'batch_id',
        'subject_id',
        'started_at',
        'ended_at',
        'title',
        'students',
        'question_stock',
        'answer_stock',
        'total_marks'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'institute_id' => 'integer',
        'academic_year_id' => 'integer',
        'course_id' => 'integer',
        'batch_id' => 'integer',
        'subject_id' => 'integer',
        'started_at' => 'datetime',
        'ended_at' => 'datetime',
        'title' => 'string',
        'students' => 'string',
        'question_stock' => 'string',
        'answer_stock' => 'string',
        'total_marks' => 'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'institute_id' => 'required',
        'academic_year_id' => 'required',
        'course_id' => 'required',
        'batch_id' => 'required',
        'subject_id' => 'required',
        'started_at' => 'required',
        'ended_at' => 'required',
        'title' => 'required|string|max:255',
        'students' => 'required|string',
        'question_stock' => 'required|string',
        'answer_stock' => 'required|string',
        'total_marks' => 'required|boolean',
        'created_at' => 'nullable',
        'updated_at' => 'nullable',
        'deleted_at' => 'nullable'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function academicYear()
    {
        return $this->belongsTo(\App\Models\AcademicYear::class, 'academic_year_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function subject()
    {
        return $this->belongsTo(\App\Models\Subject::class, 'subject_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function batch()
    {
        return $this->belongsTo(\App\Models\Batch::class, 'batch_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function institute()
    {
        return $this->belongsTo(\App\Models\Institute::class, 'institute_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function course()
    {
        return $this->belongsTo(\App\Models\Course::class, 'course_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function assesmentStudents()
    {
        return $this->hasMany(\App\Models\AssesmentStudent::class, 'assesment_id');
    }
}
