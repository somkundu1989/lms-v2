<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;


/**
 * Class IssueCategory
 * @package App\Models
 * @version December 18, 2021, 10:37 am UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection $issues
 * @property string $title
 */
class IssueCategory extends Model
{
    use SoftDeletes;


    public $table = 'issue_categories';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'title'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'title' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'title' => 'required|string|max:255',
        'created_at' => 'nullable',
        'updated_at' => 'nullable',
        'deleted_at' => 'nullable'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function issues()
    {
        return $this->hasMany(\App\Models\Issue::class, 'category_id');
    }
}
