<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;


/**
 * Class Batche
 * @package App\Models
 * @version December 18, 2021, 10:37 am UTC
 *
 * @property \App\Models\MasterAcademicYear $academicYear
 * @property \App\Models\MasterCourse $course
 * @property App\Models\Institute $institute
 * @property \Illuminate\Database\Eloquent\Collection $assesmentStudents
 * @property \Illuminate\Database\Eloquent\Collection $assesments
 * @property \Illuminate\Database\Eloquent\Collection $eduBracketStudents
 * @property \Illuminate\Database\Eloquent\Collection $eduBrackets
 * @property \Illuminate\Database\Eloquent\Collection $issues
 * @property \Illuminate\Database\Eloquent\Collection $payTransactions
 * @property integer $institute_id
 * @property integer $academic_year_id
 * @property integer $course_id
 * @property string $title
 * @property string $subjects
 */
class Batche extends Model
{
    use SoftDeletes;


    public $table = 'edu_batches';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'institute_id',
        'academic_year_id',
        'course_id',
        'title',
        'subjects'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'institute_id' => 'integer',
        'academic_year_id' => 'integer',
        'course_id' => 'integer',
        'title' => 'string',
        'subjects' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'institute_id' => 'required',
        'academic_year_id' => 'required',
        'course_id' => 'required',
        'title' => 'required|string|max:255',
        'subjects' => 'required|array',
        'created_at' => 'nullable',
        'updated_at' => 'nullable',
        'deleted_at' => 'nullable'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function academicYear()
    {
        return $this->belongsTo(\App\Models\AcademicYear::class, 'academic_year_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function course()
    {
        return $this->belongsTo(\App\Models\Course::class, 'course_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function institute()
    {
        return $this->belongsTo(\App\Models\Institute::class, 'institute_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function assesmentStudents()
    {
        return $this->hasMany(\App\Models\AssesmentStudent::class, 'batch_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function assesments()
    {
        return $this->hasMany(\App\Models\Assesment::class, 'batch_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function eduBracketStudents()
    {
        return $this->hasMany(\App\Models\BracketStudent::class, 'batch_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function brackets()
    {
        return $this->hasMany(\App\Models\Bracket::class, 'batch_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function issues()
    {
        return $this->hasMany(\App\Models\Issue::class, 'batch_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function transactions()
    {
        return $this->hasMany(\App\Models\Transaction::class, 'batch_id');
    }
}
