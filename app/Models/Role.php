<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;


/**
 * Class Role
 * @package App\Models
 * @version December 18, 2021, 2:21 pm UTC
 *
 * @property App\Models\Institute $institute
 * @property integer $institute_id
 * @property string $title
 * @property string $description
 */
class Role extends Model
{
    use SoftDeletes;


    public $table = 'org_roles';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'institute_id',
        'title',
        'description'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'institute_id' => 'integer',
        'title' => 'string',
        'description' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'institute_id' => 'required',
        'title' => 'required|string|max:255',
        'description' => 'nullable|string',
        'created_at' => 'nullable',
        'updated_at' => 'nullable',
        'deleted_at' => 'nullable'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function institute()
    {
        return $this->belongsTo(\App\Models\Institute::class, 'institute_id');
    }
}
