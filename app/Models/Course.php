<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;


/**
 * Class Course
 * @package App\Models
 * @version December 18, 2021, 10:38 am UTC
 *
 * @property \App\Models\MasterAcademicYear $academicYear
 * @property App\Models\Institute $institute
 * @property \Illuminate\Database\Eloquent\Collection $assesmentStudents
 * @property \Illuminate\Database\Eloquent\Collection $assesments
 * @property \Illuminate\Database\Eloquent\Collection $eduBatches
 * @property \Illuminate\Database\Eloquent\Collection $eduBracketStudents
 * @property \Illuminate\Database\Eloquent\Collection $eduBrackets
 * @property \Illuminate\Database\Eloquent\Collection $issues
 * @property \Illuminate\Database\Eloquent\Collection $libBooks
 * @property \Illuminate\Database\Eloquent\Collection $masterSubjects
 * @property \Illuminate\Database\Eloquent\Collection $payFees
 * @property \Illuminate\Database\Eloquent\Collection $payTransactions
 * @property integer $institute_id
 * @property integer $academic_year_id
 * @property string $title
 * @property string $description
 * @property boolean $duration
 * @property boolean $status
 */
class Course extends Model
{
    use SoftDeletes;


    public $table = 'master_courses';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'institute_id',
        'academic_year_id',
        'title',
        'description',
        'duration',
        'status'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'institute_id' => 'integer',
        'academic_year_id' => 'integer',
        'title' => 'string',
        'description' => 'string',
        'duration' => 'integer',
        'status' => 'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'institute_id' => 'required',
        'academic_year_id' => 'required',
        'title' => 'required|string|max:255',
        // 'description' => 'nullable|string',
        'duration' => 'required|integer',
        // 'status' => 'required|boolean',
        'created_at' => 'nullable',
        'updated_at' => 'nullable',
        'deleted_at' => 'nullable'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function academicYear()
    {
        return $this->belongsTo(\App\Models\AcademicYear::class, 'academic_year_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function institute()
    {
        return $this->belongsTo(\App\Models\Institute::class, 'institute_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function assesmentStudents()
    {
        return $this->hasMany(\App\Models\AssesmentStudent::class, 'course_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function assesments()
    {
        return $this->hasMany(\App\Models\Assesment::class, 'course_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function eduBatches()
    {
        return $this->hasMany(\App\Models\Batch::class, 'course_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function eduBracketStudents()
    {
        return $this->hasMany(\App\Models\BracketStudent::class, 'course_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function eduBrackets()
    {
        return $this->hasMany(\App\Models\Bracket::class, 'course_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function issues()
    {
        return $this->hasMany(\App\Models\Issue::class, 'course_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function libBooks()
    {
        return $this->hasMany(\App\Models\Book::class, 'course_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function masterSubjects()
    {
        return $this->hasMany(\App\Models\Subject::class, 'course_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function payFees()
    {
        return $this->hasMany(\App\Models\PayFee::class, 'course_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function payTransactions()
    {
        return $this->hasMany(\App\Models\PayTransaction::class, 'course_id');
    }
}
