<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

/**
 * Class User
 * @package App\Models
 * @version December 18, 2021, 2:22 pm UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection $assesmentStudents
 * @property \Illuminate\Database\Eloquent\Collection $eduBracketStudents
 * @property \Illuminate\Database\Eloquent\Collection $eduBracketStudent1s
 * @property \Illuminate\Database\Eloquent\Collection $eduBrackets
 * @property \Illuminate\Database\Eloquent\Collection $issues
 * @property \Illuminate\Database\Eloquent\Collection $issue2s
 * @property \Illuminate\Database\Eloquent\Collection $libBookAssigns
 * @property \Illuminate\Database\Eloquent\Collection $payTransactions
 * @property \Illuminate\Database\Eloquent\Collection $profiles
 * @property string $name
 * @property string $email
 * @property string|\Carbon\Carbon $email_verified_at
 * @property string $password
 * @property string $roles
 * @property string $remember_token
 */

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;
    
    public $table = 'users';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';




    public $fillable = [
        'name',
        'email',
        'password',
        'email_verified_at',
        'remember_token',
        'roles',
    ];
   
    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'email' => 'string',
        'email_verified_at' => 'datetime',
        'password' => 'string',
        'roles' => 'string',
        'remember_token' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required|string|max:255',
        'email' => 'required|string|max:255',
        // 'email_verified_at' => 'nullable',
        'password' => 'required|string|max:255',
        'roles' => 'required|array',
        // 'remember_token' => 'nullable|string|max:100',
        'created_at' => 'nullable',
        'updated_at' => 'nullable'
    ];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function assesmentStudents()
    {
        return $this->hasMany(\App\Models\AssesmentStudent::class, 'student_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function eduBracketStudents()
    {
        return $this->hasMany(\App\Models\BracketStudent::class, 'student_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function eduBracketStudent1s()
    {
        return $this->hasMany(\App\Models\BracketStudent::class, 'teacher_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function eduBrackets()
    {
        return $this->hasMany(\App\Models\Bracket::class, 'teacher_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function issues()
    {
        return $this->hasMany(\App\Models\Issue::class, 'created_by');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function issue2s()
    {
        return $this->hasMany(\App\Models\Issue::class, 'teacher_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function libBookAssigns()
    {
        return $this->hasMany(\App\Models\BookAssign::class, 'user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function payTransactions()
    {
        return $this->hasMany(\App\Models\PayTransaction::class, 'user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function profiles()
    {
        return $this->hasMany(\App\Models\Profile::class, 'user_id');
    }
}
