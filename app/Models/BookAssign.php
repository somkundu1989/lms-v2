<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;


/**
 * Class BookAssign
 * @package App\Models
 * @version December 18, 2021, 10:38 am UTC
 *
 * @property \App\Models\LibBook $book
 * @property \App\Models\User $user
 * @property \Illuminate\Database\Eloquent\Collection $payTransactions
 * @property integer $book_id
 * @property integer $user_id
 * @property string $from_date
 * @property string $to_date
 * @property string $submission_date
 * @property boolean $status
 * @property string $transaction
 */
class BookAssign extends Model
{
    use SoftDeletes;


    public $table = 'lib_book_assigns';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'book_id',
        'user_id',
        'from_date',
        'to_date',
        'submission_date',
        'status',
        'transaction'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'book_id' => 'integer',
        'user_id' => 'integer',
        'from_date' => 'date',
        'to_date' => 'date',
        'submission_date' => 'date',
        'status' => 'boolean',
        'transaction' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'book_id' => 'required',
        'user_id' => 'required',
        'from_date' => 'required',
        'to_date' => 'required',
        'submission_date' => 'nullable',
        'status' => 'required|boolean',
        'transaction' => 'nullable|string',
        'created_at' => 'nullable',
        'updated_at' => 'nullable',
        'deleted_at' => 'nullable'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function book()
    {
        return $this->belongsTo(\App\Models\Book::class, 'book_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function user()
    {
        return $this->belongsTo(\App\Models\User::class, 'user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function payTransactions()
    {
        return $this->hasMany(\App\Models\PayTransaction::class, 'book_assign_id');
    }
}
