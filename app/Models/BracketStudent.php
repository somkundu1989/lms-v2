<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;


/**
 * Class BracketStudent
 * @package App\Models
 * @version December 18, 2021, 10:37 am UTC
 *
 * @property \App\Models\MasterAcademicYear $academicYear
 * @property \App\Models\Batch $batch
 * @property \App\Models\Bracket $bracket
 * @property \App\Models\MasterCourse $course
 * @property App\Models\Institute $institute
 * @property \App\Models\User $student
 * @property \App\Models\MasterSubject $subject
 * @property \App\Models\User $teacher
 * @property integer $institute_id
 * @property integer $academic_year_id
 * @property integer $course_id
 * @property integer $batch_id
 * @property integer $bracket_id
 * @property integer $teacher_id
 * @property integer $subject_id
 * @property integer $student_id
 * @property boolean $status
 * @property string $review
 */
class BracketStudent extends Model
{
    use SoftDeletes;


    public $table = 'edu_bracket_students';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'institute_id',
        'academic_year_id',
        'course_id',
        'batch_id',
        'bracket_id',
        'teacher_id',
        'subject_id',
        'student_id',
        'status',
        'review'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'institute_id' => 'integer',
        'academic_year_id' => 'integer',
        'course_id' => 'integer',
        'batch_id' => 'integer',
        'bracket_id' => 'integer',
        'teacher_id' => 'integer',
        'subject_id' => 'integer',
        'student_id' => 'integer',
        'status' => 'boolean',
        'review' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'institute_id' => 'required',
        'academic_year_id' => 'required',
        'course_id' => 'required',
        'batch_id' => 'required',
        'bracket_id' => 'required',
        'teacher_id' => 'required',
        'subject_id' => 'required',
        'student_id' => 'required',
        'status' => 'required|boolean',
        'review' => 'nullable|string',
        'created_at' => 'nullable',
        'updated_at' => 'nullable',
        'deleted_at' => 'nullable'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function academicYear()
    {
        return $this->belongsTo(\App\Models\AcademicYear::class, 'academic_year_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function batch()
    {
        return $this->belongsTo(\App\Models\Batch::class, 'batch_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function bracket()
    {
        return $this->belongsTo(\App\Models\Bracket::class, 'bracket_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function course()
    {
        return $this->belongsTo(\App\Models\Course::class, 'course_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function institute()
    {
        return $this->belongsTo(\App\Models\Institute::class, 'institute_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function student()
    {
        return $this->belongsTo(\App\Models\User::class, 'student_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function subject()
    {
        return $this->belongsTo(\App\Models\Subject::class, 'subject_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function teacher()
    {
        return $this->belongsTo(\App\Models\User::class, 'teacher_id');
    }
}
