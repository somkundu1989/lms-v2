<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;


/**
 * Class Fee
 * @package App\Models
 * @version December 18, 2021, 10:38 am UTC
 *
 * @property \App\Models\MasterAcademicYear $academicYear
 * @property \App\Models\MasterCourse $course
 * @property App\Models\Institute $institute
 * @property \Illuminate\Database\Eloquent\Collection $payTransactions
 * @property integer $institute_id
 * @property integer $academic_year_id
 * @property integer $course_id
 * @property integer $amount
 */
class Fee extends Model
{
    use SoftDeletes;


    public $table = 'pay_fees';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'institute_id',
        'academic_year_id',
        'course_id',
        'amount'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'institute_id' => 'integer',
        'academic_year_id' => 'integer',
        'course_id' => 'integer',
        'amount' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'institute_id' => 'required',
        'academic_year_id' => 'required',
        'course_id' => 'required',
        'amount' => 'required|integer',
        'created_at' => 'nullable',
        'updated_at' => 'nullable',
        'deleted_at' => 'nullable'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function academicYear()
    {
        return $this->belongsTo(\App\Models\AcademicYear::class, 'academic_year_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function course()
    {
        return $this->belongsTo(\App\Models\Course::class, 'course_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function institute()
    {
        return $this->belongsTo(\App\Models\Institute::class, 'institute_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function payTransactions()
    {
        return $this->hasMany(\App\Models\PayTransaction::class, 'fee_id');
    }
}
