<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;


/**
 * Class issue
 * @package App\Models
 * @version December 18, 2021, 10:37 am UTC
 *
 * @property \App\Models\MasterAcademicYear $academicYear
 * @property \App\Models\Batch $batch
 * @property \App\Models\Bracket $bracket
 * @property \App\Models\IssueCategory $category
 * @property \App\Models\MasterCourse $course
 * @property \App\Models\User $createdBy
 * @property App\Models\Institute $institute
 * @property \App\Models\User $teacher
 * @property integer $id
 * @property integer $institute_id
 * @property integer $academic_year_id
 * @property integer $course_id
 * @property integer $batch_id
 * @property integer $bracket_id
 * @property integer $teacher_id
 * @property integer $created_by
 * @property integer $category_id
 * @property string $title
 * @property string $description
 * @property integer $assigned_to
 * @property string $rosolution_note
 * @property boolean $status
 * @property string|\Carbon\Carbon $initiated_at
 * @property string|\Carbon\Carbon $wip_at
 * @property string|\Carbon\Carbon $completed_at
 * @property string|\Carbon\Carbon $closed_at
 */
class issue extends Model
{
    use SoftDeletes;


    public $table = 'issues';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'id',
        'institute_id',
        'academic_year_id',
        'course_id',
        'batch_id',
        'bracket_id',
        'teacher_id',
        'created_by',
        'category_id',
        'title',
        'description',
        'assigned_to',
        'rosolution_note',
        'status',
        'initiated_at',
        'wip_at',
        'completed_at',
        'closed_at'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'institute_id' => 'integer',
        'academic_year_id' => 'integer',
        'course_id' => 'integer',
        'batch_id' => 'integer',
        'bracket_id' => 'integer',
        'teacher_id' => 'integer',
        'created_by' => 'integer',
        'category_id' => 'integer',
        'title' => 'string',
        'description' => 'string',
        'assigned_to' => 'integer',
        'rosolution_note' => 'string',
        'status' => 'boolean',
        'initiated_at' => 'datetime',
        'wip_at' => 'datetime',
        'completed_at' => 'datetime',
        'closed_at' => 'datetime'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'institute_id' => 'required',
        'academic_year_id' => 'required',
        'course_id' => 'required',
        'batch_id' => 'required',
        'bracket_id' => 'required',
        'teacher_id' => 'required',
        'created_by' => 'required',
        'category_id' => 'nullable',
        'title' => 'required|string|max:255',
        'description' => 'required|string',
        'assigned_to' => 'nullable',
        'rosolution_note' => 'nullable|string',
        'status' => 'required|boolean',
        'created_at' => 'nullable',
        'initiated_at' => 'nullable',
        'wip_at' => 'nullable',
        'completed_at' => 'nullable',
        'closed_at' => 'nullable',
        'updated_at' => 'nullable'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function academicYear()
    {
        return $this->belongsTo(\App\Models\AcademicYear::class, 'academic_year_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function batch()
    {
        return $this->belongsTo(\App\Models\Batch::class, 'batch_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function bracket()
    {
        return $this->belongsTo(\App\Models\Bracket::class, 'bracket_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function category()
    {
        return $this->belongsTo(\App\Models\IssueCategory::class, 'category_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function course()
    {
        return $this->belongsTo(\App\Models\Course::class, 'course_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function createdBy()
    {
        return $this->belongsTo(\App\Models\User::class, 'created_by');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function institute()
    {
        return $this->belongsTo(\App\Models\Institute::class, 'institute_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function teacher()
    {
        return $this->belongsTo(\App\Models\User::class, 'teacher_id');
    }
}
