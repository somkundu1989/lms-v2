<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;


/**
 * Class Book
 * @package App\Models
 * @version December 18, 2021, 10:38 am UTC
 *
 * @property \App\Models\LibAuthor $author
 * @property \App\Models\MasterCourse $course
 * @property \App\Models\LibGenre $genre
 * @property \App\Models\LibPublisher $publisher
 * @property \App\Models\MasterSubject $subject
 * @property \Illuminate\Database\Eloquent\Collection $libBookAssigns
 * @property \Illuminate\Database\Eloquent\Collection $payTransactions
 * @property integer $genre_id
 * @property integer $author_id
 * @property integer $publisher_id
 * @property integer $course_id
 * @property integer $subject_id
 * @property string $title
 * @property string $description
 * @property string $file
 */
class Book extends Model
{
    use SoftDeletes;


    public $table = 'lib_books';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'genre_id',
        'author_id',
        'publisher_id',
        'course_id',
        'subject_id',
        'title',
        'description',
        'file'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'genre_id' => 'integer',
        'author_id' => 'integer',
        'publisher_id' => 'integer',
        'course_id' => 'integer',
        'subject_id' => 'integer',
        'title' => 'string',
        'description' => 'string',
        'file' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'genre_id' => 'required',
        'author_id' => 'required',
        'publisher_id' => 'required',
        'course_id' => 'nullable',
        'subject_id' => 'nullable',
        'title' => 'required|string|max:255',
        'description' => 'nullable|string',
        'file' => 'required|string|max:255',
        'created_at' => 'nullable',
        'updated_at' => 'nullable',
        'deleted_at' => 'nullable'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function author()
    {
        return $this->belongsTo(\App\Models\Author::class, 'author_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function course()
    {
        return $this->belongsTo(\App\Models\Course::class, 'course_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function genre()
    {
        return $this->belongsTo(\App\Models\Genre::class, 'genre_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function publisher()
    {
        return $this->belongsTo(\App\Models\Publisher::class, 'publisher_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function subject()
    {
        return $this->belongsTo(\App\Models\Subject::class, 'subject_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function libBookAssigns()
    {
        return $this->hasMany(\App\Models\BookAssign::class, 'book_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function payTransactions()
    {
        return $this->hasMany(\App\Models\PayTransaction::class, 'book_id');
    }
}
