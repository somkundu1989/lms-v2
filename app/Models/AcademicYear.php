<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;


/**
 * Class AcademicYear
 * @package App\Models
 * @version December 18, 2021, 10:38 am UTC
 *
 * @property App\Models\Institute $institute
 * @property \Illuminate\Database\Eloquent\Collection $assesmentStudents
 * @property \Illuminate\Database\Eloquent\Collection $assesments
 * @property \Illuminate\Database\Eloquent\Collection $eduBatches
 * @property \Illuminate\Database\Eloquent\Collection $eduBracketStudents
 * @property \Illuminate\Database\Eloquent\Collection $eduBrackets
 * @property \Illuminate\Database\Eloquent\Collection $issues
 * @property \Illuminate\Database\Eloquent\Collection $masterCourses
 * @property \Illuminate\Database\Eloquent\Collection $masterSubjects
 * @property \Illuminate\Database\Eloquent\Collection $payFees
 * @property \Illuminate\Database\Eloquent\Collection $payTransactions
 * @property integer $institute_id
 * @property string $title
 * @property string $start_date
 * @property string $end_date
 */
class AcademicYear extends Model
{
    use SoftDeletes;


    public $table = 'master_academic_years';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'institute_id',
        'title',
        'start_date',
        'end_date'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'institute_id' => 'integer',
        'title' => 'string',
        'start_date' => 'date',
        'end_date' => 'date'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'institute_id' => 'required',
        'title' => 'required|string|max:255',
        'start_date' => 'required',
        'end_date' => 'required',
        'created_at' => 'nullable',
        'updated_at' => 'nullable',
        'deleted_at' => 'nullable'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function institute()
    {
        return $this->belongsTo(\App\Models\Institute::class, 'institute_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function assesmentStudents()
    {
        return $this->hasMany(\App\Models\AssesmentStudent::class, 'academic_year_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function assesments()
    {
        return $this->hasMany(\App\Models\Assesment::class, 'academic_year_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function batches()
    {
        return $this->hasMany(\App\Models\Batch::class, 'academic_year_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function bracketStudents()
    {
        return $this->hasMany(\App\Models\BracketStudent::class, 'academic_year_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function brackets()
    {
        return $this->hasMany(\App\Models\Bracket::class, 'academic_year_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function issues()
    {
        return $this->hasMany(\App\Models\Issue::class, 'academic_year_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function masterCourses()
    {
        return $this->hasMany(\App\Models\Course::class, 'academic_year_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    // public function masterSubjects()
    // {
    //     return $this->hasMany(\App\Models\Subject::class, 'academic_year_id');
    // }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function fees()
    {
        return $this->hasMany(\App\Models\PayFee::class, 'academic_year_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function transactions()
    {
        return $this->hasMany(\App\Models\PayTransaction::class, 'academic_year_id');
    }
}
