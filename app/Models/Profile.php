<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;


/**
 * Class Profile
 * @package App\Models
 * @version December 18, 2021, 10:39 am UTC
 *
 * @property \App\Models\User $user
 * @property integer $user_id
 * @property string $title
 * @property string $desciption
 * @property string $educations
 * @property string $experiences
 * @property string $skills
 * @property string $languages
 * @property string $testimonials
 */
class Profile extends Model
{
    use SoftDeletes;


    public $table = 'profiles';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'user_id',
        'title',
        'desciption',
        'educations',
        'experiences',
        'skills',
        'languages',
        'testimonials'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'user_id' => 'integer',
        'title' => 'string',
        'desciption' => 'string',
        'educations' => 'string',
        'experiences' => 'string',
        'skills' => 'string',
        'languages' => 'string',
        'testimonials' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'user_id' => 'required',
        'title' => 'nullable|string|max:255',
        'desciption' => 'nullable|string',
        'educations' => 'nullable|string',
        'experiences' => 'nullable|string',
        'skills' => 'nullable|string',
        'languages' => 'nullable|string',
        'testimonials' => 'nullable|string',
        'created_at' => 'nullable',
        'updated_at' => 'nullable',
        'deleted_at' => 'nullable'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function user()
    {
        return $this->belongsTo(\App\Models\User::class, 'user_id');
    }
}
