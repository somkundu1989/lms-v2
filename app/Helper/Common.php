<?php

use App\Models\Institute;
use App\Models\Role;
use App\Models\AcademicYear;
use App\Models\Course;
use App\Models\Subject;
use App\Models\Batche;

function institute($arg)
{
	return Institute::first($arg)->$arg;
}

function roles($arg = null)
{
	if ($arg == null){
		return Role::where('institute_id',institute('id'))->select('id','title')->get();	
	}
	if($arg > 0){
		return Role::where('institute_id',institute('id'))->where('id',$arg)->select('title')->first()->title;	
	}
	return Role::where('institute_id',institute('id'))->whereIn('id',json_decode($arg))->select(\DB::raw('group_concat(title) as roles'))->first()->roles;	
}

function academicYears($arg = null)
{
	if ($arg == null){
		return AcademicYear::where('institute_id',institute('id'))->select('id', \DB::raw('concat(title, " || ", start_date, " -to- ", end_date) as text'))->pluck('text','id');	
	}	
	if($arg == 0){
		return 'multiple not supported yet';	
	}	
	return AcademicYear::where('institute_id',institute('id'))->where('id',$arg)->select('id', \DB::raw('concat(title, " || ", start_date, " -to- ", end_date) as text'))->first()->text;
}

function courseDurations($arg = null)
{
	$duration = config()->get('constants.courseDuration');
	if ($arg == null){
		return $duration;
	}
	return $duration[$arg]; 
}

function courses($arg = null)
{
	if ($arg == null){
		return Course::where('institute_id',institute('id'))->select('id', 'title')->pluck('title','id');	
	}	
	if($arg > 0){
		return Course::where('institute_id',institute('id'))->where('id',$arg)->select('title')->first()->title;
	}
	return Course::where('institute_id',institute('id'))->whereIn('id',json_decode($arg))->select(\DB::raw('group_concat(title) as courses'))->first()->courses;	
}

function subjects($arg = null)
{
	if ($arg == null){
		return Subject::where('institute_id',institute('id'))->select('id', 'title')->get();	
	}	
	if($arg > 0){
		return Subject::where('institute_id',institute('id'))->where('id', $arg)->select('title')->first()->title;		
	}
	return Subject::where('institute_id',institute('id'))->whereIn('id',json_decode($arg))->select(\DB::raw('group_concat(title) as subjects'))->first()->subjects;		
}


function batches($arg = null)
{
	if ($arg == null){
		return Batche::where('institute_id',institute('id'))->select('id', 'title')->pluck('title','id');	
	}
	if($arg == 0){
		return 'multiple not supported yet';	
	}	
	return Batche::where('institute_id',institute('id'))->where('id',$arg)->select('id', 'title')->first()->title;	
}

function usersByRole($arg = null, $role = null)
{
	if ($arg == null){
		return User::where('institute_id',institute('id'))->where('roles', 'like', '%' . $role . '%')->select('id', 'name')->pluck('name','id');	
	}	
	if($arg == 0){
		return 'multiple not supported yet';	
	}	
	return User::where('institute_id',institute('id'))->where('id',$arg)->select('id', 'name')->first()->name;	
}
