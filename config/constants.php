<?php
return [
    'courseDuration' => [
		1=>'1 Month',
		2=>'2 Months',
		3=>'3 Months',
		6=>'6 Months',
		9=>'9 Months',
		12=>'12 Months (1 Year)',
		18=>'18 Months (1.5 Year)',
		24=>'24 Months (2 Years)',
		36=>'36 Months (3 Years)',
		48=>'48 Months (4 Years)',
	],
    "trueFalse" => [
        1 => "yes",
        0 => "no"
    ],
];