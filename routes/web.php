<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Auth::routes();


Route::group(['middleware' => 'auth:web',], function () {

    Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');


    Route::resource('assesmentStudents', App\Http\Controllers\AssesmentStudentController::class);


    Route::resource('assesments', App\Http\Controllers\AssesmentController::class);


    Route::resource('batches', App\Http\Controllers\BatcheController::class);


    Route::resource('bracketStudents', App\Http\Controllers\BracketStudentController::class);


    Route::resource('brackets', App\Http\Controllers\BracketController::class);


    Route::resource('issueCategories', App\Http\Controllers\IssueCategoryController::class);


    Route::resource('issues', App\Http\Controllers\issueController::class);


    Route::resource('authors', App\Http\Controllers\AuthorController::class);


    Route::resource('bookAssigns', App\Http\Controllers\BookAssignController::class);


    Route::resource('books', App\Http\Controllers\BookController::class);


    Route::resource('genres', App\Http\Controllers\GenreController::class);


    Route::resource('publishers', App\Http\Controllers\PublisherController::class);


    Route::resource('academicYears', App\Http\Controllers\AcademicYearController::class);


    Route::resource('courses', App\Http\Controllers\CourseController::class);


    Route::resource('subjects', App\Http\Controllers\SubjectController::class);


    Route::resource('fees', App\Http\Controllers\FeeController::class);


    Route::resource('transactions', App\Http\Controllers\TransactionController::class);


    Route::resource('profiles', App\Http\Controllers\ProfileController::class);


    Route::resource('institutes', App\Http\Controllers\InstituteController::class);


    Route::resource('roles', App\Http\Controllers\RoleController::class);


    Route::resource('users', App\Http\Controllers\UserController::class);
});