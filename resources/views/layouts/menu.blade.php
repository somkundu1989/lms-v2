@push('page_css')
<style type="text/css">
    

</style>
@endpush


<!-- Organization section starts -->
<li class="nav-item">
    <a class="nav-link" data-toggle="collapse" data-target="#Organization"> 
        <p>Organization</p>
    </a>
    <ul class="nav nav-list collapse {{ (Request::is('institutes*')||Request::is('roles*')) ? 'show' : '' }}"  id="Organization">
        <li class="nav-item sub-nav-item">
            <a href="{{ route('institutes.edit', [institute('id')]) }}"
               class="nav-link {{ Request::is('institutes*') ? 'active' : '' }}">
                <p>Institute</p>
            </a>
        </li>


        <li class="nav-item sub-nav-item">
            <a href="{{ route('roles.index') }}"
               class="nav-link {{ Request::is('roles*') ? 'active' : '' }}">
                <p>Roles</p>
            </a>
        </li>
    </ul>
</li>
<!-- Organization section ends -->


<!-- Users section starts -->
<li class="nav-item">
    <a href="{{ route('users.index') }}"
       class="nav-link {{ Request::is('users*') ? 'active' : '' }}">
        <p>Users</p>
    </a>
</li>
<!-- Users section ends -->


<!-- Academic section starts -->
<li class="nav-item">
    <a class="nav-link" data-toggle="collapse" data-target="#Academic"> 
        <p>Master</p>
    </a>
    <ul class="nav nav-list collapse {{ (Request::is('academicYears*')||Request::is('courses*')||Request::is('subjects*')) ? 'show' : '' }}"  id="Academic">
        <li class="nav-item sub-nav-item">
            <a href="{{ route('academicYears.index') }}"
               class="nav-link {{ Request::is('academicYears*') ? 'active' : '' }}">
                <p>Academic Years</p>
            </a>
        </li>

        <li class="nav-item sub-nav-item">
            <a href="{{ route('courses.index') }}"
               class="nav-link {{ Request::is('courses*') ? 'active' : '' }}">
                <p>Courses</p>
            </a>
        </li>

        <li class="nav-item sub-nav-item">
            <a href="{{ route('subjects.index') }}"
               class="nav-link {{ Request::is('subjects*') ? 'active' : '' }}">
                <p>Subjects</p>
            </a>
        </li>
    </ul>
</li>
<!-- Academic section ends -->


<!-- Issue section starts -->
<li class="nav-item">
    <a class="nav-link" data-toggle="collapse" data-target="#Educational"> 
        <p>Academic</p>
    </a>
    <ul class="nav nav-list collapse {{ (Request::is('batches*')||Request::is('bracketStudents*')||Request::is('brackets*')) ? 'show' : '' }}"  id="Educational">
        <li class="nav-item sub-nav-item">
            <a href="{{ route('batches.index') }}"
               class="nav-link {{ Request::is('batches*') ? 'active' : '' }}">
                <p>Batches</p>
            </a>
        </li>

        <li class="nav-item sub-nav-item">
            <a href="{{ route('brackets.index') }}"
               class="nav-link {{ Request::is('brackets*') ? 'active' : '' }}">
                <p>Brackets</p>
            </a>
        </li>

        <li class="nav-item sub-nav-item">
            <a href="{{ route('bracketStudents.index') }}"
               class="nav-link {{ Request::is('bracketStudents*') ? 'active' : '' }}">
                <p>Bracket Students</p>
            </a>
        </li>
    </ul>
</li>
<!-- Issue section ends -->


<!-- Profiles section starts -->
<li class="nav-item">
    <a href="{{ route('profiles.index') }}"
       class="nav-link {{ Request::is('profiles*') ? 'active' : '' }}">
        <p>Profiles</p>
    </a>
</li>
<!-- Profiles section ends -->


<!-- Assesment section starts -->
<li class="nav-item">
    <a class="nav-link" data-toggle="collapse" data-target="#Assesment"> 
        <p>Assesment</p>
    </a>
    <ul class="nav nav-list collapse {{ (Request::is('assesmentStudents*')||Request::is('assesments*')) ? 'show' : '' }}"  id="Assesment">

        <li class="nav-item sub-nav-item">
            <a href="{{ route('assesments.index') }}"
               class="nav-link {{ Request::is('assesments*') ? 'active' : '' }}">
                <p>Assesments</p>
            </a>
        </li>

        <li class="nav-item sub-nav-item">
            <a href="{{ route('assesmentStudents.index') }}"
               class="nav-link {{ Request::is('assesmentStudents*') ? 'active' : '' }}">
                <p>Assesment Students</p>
            </a>
        </li>
    </ul>
</li>
<!-- Assesment section ends -->


<!-- Library section starts -->
<li class="nav-item">
    <a class="nav-link" data-toggle="collapse" data-target="#Library"> 
        <p>Library</p>
    </a>
    <ul class="nav nav-list collapse {{ (Request::is('genres*')||Request::is('authors*')||Request::is('publishers*')||Request::is('books*')||Request::is('bookAssigns*')) ? 'show' : '' }}"  id="Library">
        <li class="nav-item sub-nav-item">
            <a href="{{ route('genres.index') }}"
               class="nav-link {{ Request::is('genres*') ? 'active' : '' }}">
                <p>Genres</p>
            </a>
        </li>

        <li class="nav-item sub-nav-item">
            <a href="{{ route('authors.index') }}"
               class="nav-link {{ Request::is('authors*') ? 'active' : '' }}">
                <p>Authors</p>
            </a>
        </li>

        <li class="nav-item sub-nav-item">
            <a href="{{ route('publishers.index') }}"
               class="nav-link {{ Request::is('publishers*') ? 'active' : '' }}">
                <p>Publishers</p>
            </a>
        </li>

        <li class="nav-item sub-nav-item">
            <a href="{{ route('books.index') }}"
               class="nav-link {{ Request::is('books*') ? 'active' : '' }}">
                <p>Books</p>
            </a>
        </li>

        <li class="nav-item sub-nav-item">
            <a href="{{ route('bookAssigns.index') }}"
               class="nav-link {{ Request::is('bookAssigns*') ? 'active' : '' }}">
                <p>Book Assigns</p>
            </a>
        </li>
    </ul>
</li>
<!-- Library section ends -->


<!-- Issue section starts -->
<li class="nav-item">
    <a class="nav-link" data-toggle="collapse" data-target="#Issue"> 
        <p>Issue</p>
    </a>
    <ul class="nav nav-list collapse {{ (Request::is('issueCategories*')||Request::is('issues*')) ? 'show' : '' }}"  id="Issue">
        <li class="nav-item sub-nav-item">
            <a href="{{ route('issueCategories.index') }}"
               class="nav-link {{ Request::is('issueCategories*') ? 'active' : '' }}">
                <p>Issue Categories</p>
            </a>
        </li>

        <li class="nav-item sub-nav-item">
            <a href="{{ route('issues.index') }}"
               class="nav-link {{ Request::is('issues*') ? 'active' : '' }}">
                <p>Issues</p>
            </a>
        </li>
    </ul>
</li>
<!-- Issue section ends -->


<!-- Payment section starts -->
<li class="nav-item">
    <a class="nav-link" data-toggle="collapse" data-target="#Payment"> 
        <p>Payment</p>
    </a>
    <ul class="nav nav-list collapse {{ (Request::is('transactions*')||Request::is('fees*')) ? 'show' : '' }}"  id="Payment">
        <li class="nav-item sub-nav-item">
            <a href="{{ route('fees.index') }}"
               class="nav-link {{ Request::is('fees*') ? 'active' : '' }}">
                <p>Fees</p>
            </a>
        </li>

        <li class="nav-item sub-nav-item">
            <a href="{{ route('transactions.index') }}"
               class="nav-link {{ Request::is('transactions*') ? 'active' : '' }}">
                <p>Transactions</p>
            </a>
        </li>
    </ul>
</li>
<!-- Payment section ends -->


<!-- logout section starts -->
<form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
  @csrf
</form>

<li class="nav-item">
  <a href="#" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" class="nav-link ">
    <p> Logout </p>
  </a>
</li>
<!-- logout section ends -->