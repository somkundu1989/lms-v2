<!-- Institute Id Field -->
<div class="col-sm-12">
    {!! Form::label('institute_id', 'Institute Id:') !!}
    <p>{{ $assesment->institute_id }}</p>
</div>

<!-- Academic Year Id Field -->
<div class="col-sm-12">
    {!! Form::label('academic_year_id', 'Academic Year Id:') !!}
    <p>{{ $assesment->academic_year_id }}</p>
</div>

<!-- Course Id Field -->
<div class="col-sm-12">
    {!! Form::label('course_id', 'Course Id:') !!}
    <p>{{ $assesment->course_id }}</p>
</div>

<!-- Batch Id Field -->
<div class="col-sm-12">
    {!! Form::label('batch_id', 'Batch Id:') !!}
    <p>{{ $assesment->batch_id }}</p>
</div>

<!-- Subject Id Field -->
<div class="col-sm-12">
    {!! Form::label('subject_id', 'Subject Id:') !!}
    <p>{{ $assesment->subject_id }}</p>
</div>

<!-- Started At Field -->
<div class="col-sm-12">
    {!! Form::label('started_at', 'Started At:') !!}
    <p>{{ $assesment->started_at }}</p>
</div>

<!-- Ended At Field -->
<div class="col-sm-12">
    {!! Form::label('ended_at', 'Ended At:') !!}
    <p>{{ $assesment->ended_at }}</p>
</div>

<!-- Title Field -->
<div class="col-sm-12">
    {!! Form::label('title', 'Title:') !!}
    <p>{{ $assesment->title }}</p>
</div>

<!-- Students Field -->
<div class="col-sm-12">
    {!! Form::label('students', 'Students:') !!}
    <p>{{ $assesment->students }}</p>
</div>

<!-- Question Stock Field -->
<div class="col-sm-12">
    {!! Form::label('question_stock', 'Question Stock:') !!}
    <p>{{ $assesment->question_stock }}</p>
</div>

<!-- Answer Stock Field -->
<div class="col-sm-12">
    {!! Form::label('answer_stock', 'Answer Stock:') !!}
    <p>{{ $assesment->answer_stock }}</p>
</div>

<!-- Total Marks Field -->
<div class="col-sm-12">
    {!! Form::label('total_marks', 'Total Marks:') !!}
    <p>{{ $assesment->total_marks }}</p>
</div>

