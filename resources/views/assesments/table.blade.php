<div class="table-responsive">
    <table class="table" id="assesments-table">
        <thead>
        <tr>
        <th>Academic Year Id</th>
        <th>Course Id</th>
        <th>Batch Id</th>
        <th>Subject Id</th>
        <th>Started At</th>
        <th>Ended At</th>
        <th>Title</th>
        <th>Students</th>
        <th>Question Stock</th>
        <th>Answer Stock</th>
        <th>Total Marks</th>
            <th colspan="3">Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($assesments as $assesment)
            <tr>
            <td>{{ $assesment->academic_year_id }}</td>
            <td>{{ $assesment->course_id }}</td>
            <td>{{ $assesment->batch_id }}</td>
            <td>{{ $assesment->subject_id }}</td>
            <td>{{ $assesment->started_at }}</td>
            <td>{{ $assesment->ended_at }}</td>
            <td>{{ $assesment->title }}</td>
            <td>{{ $assesment->students }}</td>
            <td>{{ $assesment->question_stock }}</td>
            <td>{{ $assesment->answer_stock }}</td>
            <td>{{ $assesment->total_marks }}</td>
                <td width="120">
                    {!! Form::open(['route' => ['assesments.destroy', $assesment->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('assesments.show', [$assesment->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-eye"></i>
                        </a>
                        <a href="{{ route('assesments.edit', [$assesment->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-edit"></i>
                        </a>
                        {!! Form::button('<i class="far fa-trash-alt"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
