<input type="hidden" name="institute_id" value="{{ institute('id') }}">
{{--
<!-- Institute Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('institute_id', 'Institute Id:') !!}
    {!! Form::number('institute_id', null, ['class' => 'form-control']) !!}
</div>
--}}

<!-- Academic Year Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('academic_year_id', 'Academic Year Id:') !!}
    {!! Form::number('academic_year_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Course Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('course_id', 'Course Id:') !!}
    {!! Form::number('course_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Batch Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('batch_id', 'Batch Id:') !!}
    {!! Form::number('batch_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Subject Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('subject_id', 'Subject Id:') !!}
    {!! Form::number('subject_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Started At Field -->
<div class="form-group col-sm-6">
    {!! Form::label('started_at', 'Started At:') !!}
    {!! Form::text('started_at', null, ['class' => 'form-control','id'=>'started_at']) !!}
</div>

@push('page_scripts')
    <script type="text/javascript">
        $('#started_at').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: true,
            sideBySide: true
        })
    </script>
@endpush

<!-- Ended At Field -->
<div class="form-group col-sm-6">
    {!! Form::label('ended_at', 'Ended At:') !!}
    {!! Form::text('ended_at', null, ['class' => 'form-control','id'=>'ended_at']) !!}
</div>

@push('page_scripts')
    <script type="text/javascript">
        $('#ended_at').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: true,
            sideBySide: true
        })
    </script>
@endpush

<!-- Title Field -->
<div class="form-group col-sm-6">
    {!! Form::label('title', 'Title:') !!}
    {!! Form::text('title', null, ['class' => 'form-control','maxlength' => 255,'maxlength' => 255]) !!}
</div>

<!-- Students Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('students', 'Students:') !!}
    {!! Form::textarea('students', null, ['class' => 'form-control','rows' => 2]) !!}
</div>

<!-- Question Stock Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('question_stock', 'Question Stock:') !!}
    {!! Form::textarea('question_stock', null, ['class' => 'form-control','rows' => 2]) !!}
</div>

<!-- Answer Stock Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('answer_stock', 'Answer Stock:') !!}
    {!! Form::textarea('answer_stock', null, ['class' => 'form-control','rows' => 2]) !!}
</div>

<!-- Total Marks Field -->
<div class="form-group col-sm-6">
    <div class="form-check">
        {!! Form::hidden('total_marks', 0, ['class' => 'form-check-input']) !!}
        {!! Form::checkbox('total_marks', '1', null, ['class' => 'form-check-input']) !!}
        {!! Form::label('total_marks', 'Total Marks', ['class' => 'form-check-label']) !!}
    </div>
</div>
