<input type="hidden" name="institute_id" value="{{ institute('id') }}">
{{--
<!-- Institute Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('institute_id', 'Institute Id:') !!}
    {!! Form::number('institute_id', null, ['class' => 'form-control']) !!}
</div>
--}}

<!-- Academic Year Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('academic_year_id', 'Academic Year Id:') !!}
    {!! Form::select('academic_year_id', academicYears(), null, ['class' => 'form-control']) !!}
</div>

<!-- Course Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('course_id', 'Course Id:') !!}
    {!! Form::select('course_id', courses(), null, ['class' => 'form-control', ]) !!}
</div>

<!-- Subjects Field -->
<div class="form-group col-sm-6">
    {!! Form::label('subjects', 'Subjects:') !!}

    <select class="form-control" multiple name="subjects[]">
        @foreach(subjects() as $obj)
            <option value="{{$obj->id}}" {{ ((!empty($batch))?(((int)stripos($batch->subjects, '"'.$obj->id.'"'))>0 ? 'selected="selected"' : ''): '') }} >{{$obj->title}}</option>
        @endforeach
    </select>
</div>

<!-- Title Field -->
<div class="form-group col-sm-6">
    {!! Form::label('title', 'Title:') !!}
    {!! Form::text('title', null, ['class' => 'form-control','maxlength' => 255,'maxlength' => 255]) !!}
</div>
