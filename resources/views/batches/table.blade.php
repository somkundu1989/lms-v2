<div class="table-responsive">
    <table class="table" id="batches-table">
        <thead>
        <tr>
        <th>Academic Year</th>
        <th>Course</th>
        <th>Title</th>
        <th>Subjects</th>
            <th colspan="3">Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($batches as $batche)
            <tr>
            <td>{{ academicYears($batche->academic_year_id) }}</td>
            <td>{{ courses($batche->course_id) }}</td>
            <td>{{ $batche->title }}</td>
            <td>{{ subjects($batche->subjects) }}</td>
                <td width="120">
                    {!! Form::open(['route' => ['batches.destroy', $batche->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('batches.show', [$batche->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-eye"></i>
                        </a>
                        <a href="{{ route('batches.edit', [$batche->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-edit"></i>
                        </a>
                        {!! Form::button('<i class="far fa-trash-alt"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
