<!-- Institute Id Field -->
<div class="col-sm-12">
    {!! Form::label('institute_id', 'Institute Id:') !!}
    <p>{{ $batche->institute_id }}</p>
</div>

<!-- Academic Year Id Field -->
<div class="col-sm-12">
    {!! Form::label('academic_year_id', 'Academic Year Id:') !!}
    <p>{{ $batche->academic_year_id }}</p>
</div>

<!-- Course Id Field -->
<div class="col-sm-12">
    {!! Form::label('course_id', 'Course Id:') !!}
    <p>{{ $batche->course_id }}</p>
</div>

<!-- Title Field -->
<div class="col-sm-12">
    {!! Form::label('title', 'Title:') !!}
    <p>{{ $batche->title }}</p>
</div>

<!-- Subjects Field -->
<div class="col-sm-12">
    {!! Form::label('subjects', 'Subjects:') !!}
    <p>{{ $batche->subjects }}</p>
</div>

