<!-- Id Field -->
<div class="col-sm-12">
    {!! Form::label('id', 'Id:') !!}
    <p>{{ $issue->id }}</p>
</div>

<!-- Institute Id Field -->
<div class="col-sm-12">
    {!! Form::label('institute_id', 'Institute Id:') !!}
    <p>{{ $issue->institute_id }}</p>
</div>

<!-- Academic Year Id Field -->
<div class="col-sm-12">
    {!! Form::label('academic_year_id', 'Academic Year Id:') !!}
    <p>{{ $issue->academic_year_id }}</p>
</div>

<!-- Course Id Field -->
<div class="col-sm-12">
    {!! Form::label('course_id', 'Course Id:') !!}
    <p>{{ $issue->course_id }}</p>
</div>

<!-- Batch Id Field -->
<div class="col-sm-12">
    {!! Form::label('batch_id', 'Batch Id:') !!}
    <p>{{ $issue->batch_id }}</p>
</div>

<!-- Bracket Id Field -->
<div class="col-sm-12">
    {!! Form::label('bracket_id', 'Bracket Id:') !!}
    <p>{{ $issue->bracket_id }}</p>
</div>

<!-- Teacher Id Field -->
<div class="col-sm-12">
    {!! Form::label('teacher_id', 'Teacher Id:') !!}
    <p>{{ $issue->teacher_id }}</p>
</div>

<!-- Created By Field -->
<div class="col-sm-12">
    {!! Form::label('created_by', 'Created By:') !!}
    <p>{{ $issue->created_by }}</p>
</div>

<!-- Category Id Field -->
<div class="col-sm-12">
    {!! Form::label('category_id', 'Category Id:') !!}
    <p>{{ $issue->category_id }}</p>
</div>

<!-- Title Field -->
<div class="col-sm-12">
    {!! Form::label('title', 'Title:') !!}
    <p>{{ $issue->title }}</p>
</div>

<!-- Description Field -->
<div class="col-sm-12">
    {!! Form::label('description', 'Description:') !!}
    <p>{{ $issue->description }}</p>
</div>

<!-- Assigned To Field -->
<div class="col-sm-12">
    {!! Form::label('assigned_to', 'Assigned To:') !!}
    <p>{{ $issue->assigned_to }}</p>
</div>

<!-- Rosolution Note Field -->
<div class="col-sm-12">
    {!! Form::label('rosolution_note', 'Rosolution Note:') !!}
    <p>{{ $issue->rosolution_note }}</p>
</div>

<!-- Status Field -->
<div class="col-sm-12">
    {!! Form::label('status', 'Status:') !!}
    <p>{{ $issue->status }}</p>
</div>

<!-- Initiated At Field -->
<div class="col-sm-12">
    {!! Form::label('initiated_at', 'Initiated At:') !!}
    <p>{{ $issue->initiated_at }}</p>
</div>

<!-- Wip At Field -->
<div class="col-sm-12">
    {!! Form::label('wip_at', 'Wip At:') !!}
    <p>{{ $issue->wip_at }}</p>
</div>

<!-- Completed At Field -->
<div class="col-sm-12">
    {!! Form::label('completed_at', 'Completed At:') !!}
    <p>{{ $issue->completed_at }}</p>
</div>

<!-- Closed At Field -->
<div class="col-sm-12">
    {!! Form::label('closed_at', 'Closed At:') !!}
    <p>{{ $issue->closed_at }}</p>
</div>

