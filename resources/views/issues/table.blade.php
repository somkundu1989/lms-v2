<div class="table-responsive">
    <table class="table" id="issues-table">
        <thead>
        <tr>
        <th>Academic Year Id</th>
        <th>Course Id</th>
        <th>Batch Id</th>
        <th>Bracket Id</th>
        <th>Teacher Id</th>
        <th>Created By</th>
        <th>Category Id</th>
        <th>Title</th>
        <th>Description</th>
        <th>Assigned To</th>
        <th>Rosolution Note</th>
        <th>Status</th>
        <th>Initiated At</th>
        <th>Wip At</th>
        <th>Completed At</th>
        <th>Closed At</th>
            <th colspan="3">Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($issues as $issue)
            <tr>
            <td>{{ $issue->academic_year_id }}</td>
            <td>{{ $issue->course_id }}</td>
            <td>{{ $issue->batch_id }}</td>
            <td>{{ $issue->bracket_id }}</td>
            <td>{{ $issue->teacher_id }}</td>
            <td>{{ $issue->created_by }}</td>
            <td>{{ $issue->category_id }}</td>
            <td>{{ $issue->title }}</td>
            <td>{{ $issue->description }}</td>
            <td>{{ $issue->assigned_to }}</td>
            <td>{{ $issue->rosolution_note }}</td>
            <td>{{ $issue->status }}</td>
            <td>{{ $issue->initiated_at }}</td>
            <td>{{ $issue->wip_at }}</td>
            <td>{{ $issue->completed_at }}</td>
            <td>{{ $issue->closed_at }}</td>
                <td width="120">
                    {!! Form::open(['route' => ['issues.destroy', $issue->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('issues.show', [$issue->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-eye"></i>
                        </a>
                        <a href="{{ route('issues.edit', [$issue->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-edit"></i>
                        </a>
                        {!! Form::button('<i class="far fa-trash-alt"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
