<input type="hidden" name="institute_id" value="{{ institute('id') }}">
{{--
<!-- Institute Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('institute_id', 'Institute Id:') !!}
    {!! Form::number('institute_id', null, ['class' => 'form-control']) !!}
</div>
--}}

<!-- Academic Year Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('academic_year_id', 'Academic Year Id:') !!}
    {!! Form::number('academic_year_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Course Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('course_id', 'Course Id:') !!}
    {!! Form::number('course_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Batch Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('batch_id', 'Batch Id:') !!}
    {!! Form::number('batch_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Bracket Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('bracket_id', 'Bracket Id:') !!}
    {!! Form::number('bracket_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Teacher Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('teacher_id', 'Teacher Id:') !!}
    {!! Form::number('teacher_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Created By Field -->
<div class="form-group col-sm-6">
    {!! Form::label('created_by', 'Created By:') !!}
    {!! Form::number('created_by', null, ['class' => 'form-control']) !!}
</div>

<!-- Category Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('category_id', 'Category Id:') !!}
    {!! Form::number('category_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Title Field -->
<div class="form-group col-sm-6">
    {!! Form::label('title', 'Title:') !!}
    {!! Form::text('title', null, ['class' => 'form-control','maxlength' => 255,'maxlength' => 255]) !!}
</div>

<!-- Description Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('description', 'Description:') !!}
    {!! Form::textarea('description', null, ['class' => 'form-control','rows' => 2]) !!}
</div>

<!-- Assigned To Field -->
<div class="form-group col-sm-6">
    {!! Form::label('assigned_to', 'Assigned To:') !!}
    {!! Form::number('assigned_to', null, ['class' => 'form-control']) !!}
</div>

<!-- Rosolution Note Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('rosolution_note', 'Rosolution Note:') !!}
    {!! Form::textarea('rosolution_note', null, ['class' => 'form-control','rows' => 2]) !!}
</div>

<!-- Status Field -->
<div class="form-group col-sm-6">
    <div class="form-check">
        {!! Form::hidden('status', 0, ['class' => 'form-check-input']) !!}
        {!! Form::checkbox('status', '1', null, ['class' => 'form-check-input']) !!}
        {!! Form::label('status', 'Status', ['class' => 'form-check-label']) !!}
    </div>
</div>


<!-- Initiated At Field -->
<div class="form-group col-sm-6">
    {!! Form::label('initiated_at', 'Initiated At:') !!}
    {!! Form::text('initiated_at', null, ['class' => 'form-control','id'=>'initiated_at']) !!}
</div>

@push('page_scripts')
    <script type="text/javascript">
        $('#initiated_at').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: true,
            sideBySide: true
        })
    </script>
@endpush

<!-- Wip At Field -->
<div class="form-group col-sm-6">
    {!! Form::label('wip_at', 'Wip At:') !!}
    {!! Form::text('wip_at', null, ['class' => 'form-control','id'=>'wip_at']) !!}
</div>

@push('page_scripts')
    <script type="text/javascript">
        $('#wip_at').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: true,
            sideBySide: true
        })
    </script>
@endpush

<!-- Completed At Field -->
<div class="form-group col-sm-6">
    {!! Form::label('completed_at', 'Completed At:') !!}
    {!! Form::text('completed_at', null, ['class' => 'form-control','id'=>'completed_at']) !!}
</div>

@push('page_scripts')
    <script type="text/javascript">
        $('#completed_at').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: true,
            sideBySide: true
        })
    </script>
@endpush

<!-- Closed At Field -->
<div class="form-group col-sm-6">
    {!! Form::label('closed_at', 'Closed At:') !!}
    {!! Form::text('closed_at', null, ['class' => 'form-control','id'=>'closed_at']) !!}
</div>

@push('page_scripts')
    <script type="text/javascript">
        $('#closed_at').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: true,
            sideBySide: true
        })
    </script>
@endpush