<div class="table-responsive">
    <table class="table" id="brackets-table">
        <thead>
        <tr>
        <th>Academic Year Id</th>
        <th>Course Id</th>
        <th>Batch Id</th>
        <th>Started At</th>
        <th>Ended At</th>
        <th>Teacher Id</th>
        <th>Subject Id</th>
        <th>Title</th>
        <th>Students</th>
        <th>Meeting Link</th>
            <th colspan="3">Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($brackets as $bracket)
            <tr>
            <td>{{ $bracket->academic_year_id }}</td>
            <td>{{ $bracket->course_id }}</td>
            <td>{{ $bracket->batch_id }}</td>
            <td>{{ $bracket->started_at }}</td>
            <td>{{ $bracket->ended_at }}</td>
            <td>{{ $bracket->teacher_id }}</td>
            <td>{{ $bracket->subject_id }}</td>
            <td>{{ $bracket->title }}</td>
            <td>{{ $bracket->students }}</td>
            <td>{{ $bracket->meeting_link }}</td>
                <td width="120">
                    {!! Form::open(['route' => ['brackets.destroy', $bracket->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('brackets.show', [$bracket->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-eye"></i>
                        </a>
                        <a href="{{ route('brackets.edit', [$bracket->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-edit"></i>
                        </a>
                        {!! Form::button('<i class="far fa-trash-alt"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
