<input type="hidden" name="institute_id" value="{{ institute('id') }}">
{{--
<!-- Institute Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('institute_id', 'Institute Id:') !!}
    {!! Form::number('institute_id', null, ['class' => 'form-control']) !!}
</div>
--}}

<!-- Academic Year Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('academic_year_id', 'Academic Year Id:') !!}
    {!! Form::select('academic_year_id', academicYears(), null, ['class' => 'form-control']) !!}
</div>

<!-- Course Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('course_id', 'Course Id:') !!}
    {!! Form::select('course_id', courses(), null, ['class' => 'form-control']) !!}
</div>

<!-- Batch Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('batch_id', 'Batch Id:') !!}
    {!! Form::select('batch_id', batches(), null, ['class' => 'form-control']) !!}
</div>

<!-- Started At Field -->
<div class="form-group col-sm-6">
    {!! Form::label('started_at', 'Started At:') !!}
    {!! Form::text('started_at', null, ['class' => 'form-control','id'=>'started_at']) !!}
</div>

@push('page_scripts')
    <script type="text/javascript">
        $('#started_at').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: true,
            sideBySide: true
        })
    </script>
@endpush

<!-- Ended At Field -->
<div class="form-group col-sm-6">
    {!! Form::label('ended_at', 'Ended At:') !!}
    {!! Form::text('ended_at', null, ['class' => 'form-control','id'=>'ended_at']) !!}
</div>

@push('page_scripts')
    <script type="text/javascript">
        $('#ended_at').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: true,
            sideBySide: true
        })
    </script>
@endpush

<!-- Teacher Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('teacher_id', 'Teacher Id:') !!}
    {!! Form::number('teacher_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Subject Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('subject_id', 'Subject Id:') !!}
    {!! Form::number('subject_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Title Field -->
<div class="form-group col-sm-6">
    {!! Form::label('title', 'Title:') !!}
    {!! Form::text('title', null, ['class' => 'form-control','maxlength' => 255,'maxlength' => 255]) !!}
</div>

<!-- Students Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('students', 'Students:') !!}
    {!! Form::textarea('students', null, ['class' => 'form-control','rows' => 2]) !!}
</div>

<!-- Meeting Link Field -->
<div class="form-group col-sm-6">
    {!! Form::label('meeting_link', 'Meeting Link:') !!}
    {!! Form::text('meeting_link', null, ['class' => 'form-control','maxlength' => 255,'maxlength' => 255]) !!}
</div>