<!-- Institute Id Field -->
<div class="col-sm-12">
    {!! Form::label('institute_id', 'Institute Id:') !!}
    <p>{{ $bracket->institute_id }}</p>
</div>

<!-- Academic Year Id Field -->
<div class="col-sm-12">
    {!! Form::label('academic_year_id', 'Academic Year Id:') !!}
    <p>{{ $bracket->academic_year_id }}</p>
</div>

<!-- Course Id Field -->
<div class="col-sm-12">
    {!! Form::label('course_id', 'Course Id:') !!}
    <p>{{ $bracket->course_id }}</p>
</div>

<!-- Batch Id Field -->
<div class="col-sm-12">
    {!! Form::label('batch_id', 'Batch Id:') !!}
    <p>{{ $bracket->batch_id }}</p>
</div>

<!-- Started At Field -->
<div class="col-sm-12">
    {!! Form::label('started_at', 'Started At:') !!}
    <p>{{ $bracket->started_at }}</p>
</div>

<!-- Ended At Field -->
<div class="col-sm-12">
    {!! Form::label('ended_at', 'Ended At:') !!}
    <p>{{ $bracket->ended_at }}</p>
</div>

<!-- Teacher Id Field -->
<div class="col-sm-12">
    {!! Form::label('teacher_id', 'Teacher Id:') !!}
    <p>{{ $bracket->teacher_id }}</p>
</div>

<!-- Subject Id Field -->
<div class="col-sm-12">
    {!! Form::label('subject_id', 'Subject Id:') !!}
    <p>{{ $bracket->subject_id }}</p>
</div>

<!-- Title Field -->
<div class="col-sm-12">
    {!! Form::label('title', 'Title:') !!}
    <p>{{ $bracket->title }}</p>
</div>

<!-- Students Field -->
<div class="col-sm-12">
    {!! Form::label('students', 'Students:') !!}
    <p>{{ $bracket->students }}</p>
</div>

<!-- Meeting Link Field -->
<div class="col-sm-12">
    {!! Form::label('meeting_link', 'Meeting Link:') !!}
    <p>{{ $bracket->meeting_link }}</p>
</div>

