<input type="hidden" name="institute_id" value="{{ institute('id') }}">
{{--
<!-- Institute Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('institute_id', 'Institute Id:') !!}
    {!! Form::number('institute_id', null, ['class' => 'form-control']) !!}
</div>
--}}

<!-- Academic Year Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('academic_year_id', 'Academic Year Id:') !!}
    {!! Form::select('academic_year_id', academicYears(), null, ['class' => 'form-control']) !!}
</div>

<!-- Title Field -->
<div class="form-group col-sm-6">
    {!! Form::label('title', 'Title:') !!}
    {!! Form::text('title', null, ['class' => 'form-control','maxlength' => 255,'maxlength' => 255]) !!}
</div>

<!-- Description Field -->
<div class="form-group col-sm-6">
    {!! Form::label('description', 'Description:') !!}
    {!! Form::textarea('description', null, ['class' => 'form-control','rows' => 2]) !!}
</div>

<!-- Duration Field -->
<div class="form-group col-sm-6">
    {!! Form::label('duration', 'Duration:') !!}
    {!! Form::select('duration', courseDurations(), null, ['class' => 'form-control']) !!}
</div>


<!-- Status Field -->
<div class="form-group col-sm-6">
    <div class="form-check">
        {!! Form::hidden('status', 1) !!}
    </div>
</div>
