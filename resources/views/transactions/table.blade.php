<div class="table-responsive">
    <table class="table" id="transactions-table">
        <thead>
        <tr>
        <th>Academic Year Id</th>
        <th>Fee Id</th>
        <th>User Id</th>
        <th>Course Id</th>
        <th>Batch Id</th>
        <th>Book Id</th>
        <th>Book Assign Id</th>
        <th>Amount</th>
        <th>From Date</th>
        <th>To Date</th>
            <th colspan="3">Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($transactions as $transaction)
            <tr>
            <td>{{ $transaction->academic_year_id }}</td>
            <td>{{ $transaction->fee_id }}</td>
            <td>{{ $transaction->user_id }}</td>
            <td>{{ $transaction->course_id }}</td>
            <td>{{ $transaction->batch_id }}</td>
            <td>{{ $transaction->book_id }}</td>
            <td>{{ $transaction->book_assign_id }}</td>
            <td>{{ $transaction->amount }}</td>
            <td>{{ $transaction->from_date }}</td>
            <td>{{ $transaction->to_date }}</td>
                <td width="120">
                    {!! Form::open(['route' => ['transactions.destroy', $transaction->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('transactions.show', [$transaction->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-eye"></i>
                        </a>
                        <a href="{{ route('transactions.edit', [$transaction->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-edit"></i>
                        </a>
                        {!! Form::button('<i class="far fa-trash-alt"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
