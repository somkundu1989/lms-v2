<!-- Institute Id Field -->
<div class="col-sm-12">
    {!! Form::label('institute_id', 'Institute Id:') !!}
    <p>{{ $transaction->institute_id }}</p>
</div>

<!-- Academic Year Id Field -->
<div class="col-sm-12">
    {!! Form::label('academic_year_id', 'Academic Year Id:') !!}
    <p>{{ $transaction->academic_year_id }}</p>
</div>

<!-- Fee Id Field -->
<div class="col-sm-12">
    {!! Form::label('fee_id', 'Fee Id:') !!}
    <p>{{ $transaction->fee_id }}</p>
</div>

<!-- User Id Field -->
<div class="col-sm-12">
    {!! Form::label('user_id', 'User Id:') !!}
    <p>{{ $transaction->user_id }}</p>
</div>

<!-- Course Id Field -->
<div class="col-sm-12">
    {!! Form::label('course_id', 'Course Id:') !!}
    <p>{{ $transaction->course_id }}</p>
</div>

<!-- Batch Id Field -->
<div class="col-sm-12">
    {!! Form::label('batch_id', 'Batch Id:') !!}
    <p>{{ $transaction->batch_id }}</p>
</div>

<!-- Book Id Field -->
<div class="col-sm-12">
    {!! Form::label('book_id', 'Book Id:') !!}
    <p>{{ $transaction->book_id }}</p>
</div>

<!-- Book Assign Id Field -->
<div class="col-sm-12">
    {!! Form::label('book_assign_id', 'Book Assign Id:') !!}
    <p>{{ $transaction->book_assign_id }}</p>
</div>

<!-- Amount Field -->
<div class="col-sm-12">
    {!! Form::label('amount', 'Amount:') !!}
    <p>{{ $transaction->amount }}</p>
</div>

<!-- From Date Field -->
<div class="col-sm-12">
    {!! Form::label('from_date', 'From Date:') !!}
    <p>{{ $transaction->from_date }}</p>
</div>

<!-- To Date Field -->
<div class="col-sm-12">
    {!! Form::label('to_date', 'To Date:') !!}
    <p>{{ $transaction->to_date }}</p>
</div>

