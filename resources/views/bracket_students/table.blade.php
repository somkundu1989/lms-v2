<div class="table-responsive">
    <table class="table" id="bracketStudents-table">
        <thead>
        <tr>
        <th>Academic Year Id</th>
        <th>Course Id</th>
        <th>Batch Id</th>
        <th>Bracket Id</th>
        <th>Teacher Id</th>
        <th>Subject Id</th>
        <th>Student Id</th>
        <th>Status</th>
        <th>Review</th>
            <th colspan="3">Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($bracketStudents as $bracketStudent)
            <tr>
            <td>{{ $bracketStudent->academic_year_id }}</td>
            <td>{{ $bracketStudent->course_id }}</td>
            <td>{{ $bracketStudent->batch_id }}</td>
            <td>{{ $bracketStudent->bracket_id }}</td>
            <td>{{ $bracketStudent->teacher_id }}</td>
            <td>{{ $bracketStudent->subject_id }}</td>
            <td>{{ $bracketStudent->student_id }}</td>
            <td>{{ $bracketStudent->status }}</td>
            <td>{{ $bracketStudent->review }}</td>
                <td width="120">
                    {!! Form::open(['route' => ['bracketStudents.destroy', $bracketStudent->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('bracketStudents.show', [$bracketStudent->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-eye"></i>
                        </a>
                        <a href="{{ route('bracketStudents.edit', [$bracketStudent->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-edit"></i>
                        </a>
                        {!! Form::button('<i class="far fa-trash-alt"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
