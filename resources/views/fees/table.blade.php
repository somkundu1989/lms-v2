<div class="table-responsive">
    <table class="table" id="fees-table">
        <thead>
        <tr>
        <th>Academic Year Id</th>
        <th>Course Id</th>
        <th>Amount</th>
            <th colspan="3">Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($fees as $fee)
            <tr>
            <td>{{ $fee->academic_year_id }}</td>
            <td>{{ $fee->course_id }}</td>
            <td>{{ $fee->amount }}</td>
                <td width="120">
                    {!! Form::open(['route' => ['fees.destroy', $fee->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('fees.show', [$fee->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-eye"></i>
                        </a>
                        <a href="{{ route('fees.edit', [$fee->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-edit"></i>
                        </a>
                        {!! Form::button('<i class="far fa-trash-alt"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
