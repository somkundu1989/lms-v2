<div class="table-responsive">
    <table class="table" id="institutes-table">
        <thead>
        <tr>
            <th>Logo</th>
        <th>Title</th>
        <th>Establishmint Date</th>
            <th colspan="3">Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($institutes as $institute)
            <tr>
                <td>{{ $institute->logo }}</td>
            <td>{{ $institute->title }}</td>
            <td>{{ $institute->establishmint_date }}</td>
                <td width="120">
                    {!! Form::open(['route' => ['institutes.destroy', $institute->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('institutes.show', [$institute->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-eye"></i>
                        </a>
                        <a href="{{ route('institutes.edit', [$institute->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-edit"></i>
                        </a>
                        {!! Form::button('<i class="far fa-trash-alt"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
