<!-- Logo Field -->
<div class="col-sm-12">
    {!! Form::label('logo', 'Logo:') !!}
    <p>{{ $institute->logo }}</p>
</div>

<!-- Title Field -->
<div class="col-sm-12">
    {!! Form::label('title', 'Title:') !!}
    <p>{{ $institute->title }}</p>
</div>

<!-- Establishmint Date Field -->
<div class="col-sm-12">
    {!! Form::label('establishmint_date', 'Establishmint Date:') !!}
    <p>{{ $institute->establishmint_date }}</p>
</div>

