<!-- Logo Field -->
<div class="form-group col-sm-6">
    {!! Form::label('logo', 'Logo:') !!}
    <input type="file" class="form-control" name="logo" />
    <br />
    <img src="{{ asset($institute->logo) }}" height="30" />
</div>

<!-- Title Field -->
<div class="form-group col-sm-6">
    {!! Form::label('title', 'Title:') !!}
    {!! Form::text('title', null, ['class' => 'form-control','maxlength' => 255,'maxlength' => 255]) !!}
</div>

<!-- Establishmint Date Field -->
<div class="form-group col-sm-6">
    {!! Form::label('establishmint_date', 'Establishmint Date:') !!}
    {!! Form::text('establishmint_date', null, ['class' => 'form-control establishmint_date']) !!}
</div>

@push('page_scripts')
    <script type="text/javascript">
        $('.establishmint_date').datetimepicker({
            format: 'YYYY-MM-DD',
            useCurrent: true,
            sideBySide: true
        })
    </script>
@endpush