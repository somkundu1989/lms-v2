<div class="table-responsive">
    <table class="table" id="bookAssigns-table">
        <thead>
        <tr>
            <th>Book Id</th>
        <th>User Id</th>
        <th>From Date</th>
        <th>To Date</th>
        <th>Submission Date</th>
        <th>Status</th>
        <th>Transaction</th>
            <th colspan="3">Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($bookAssigns as $bookAssign)
            <tr>
                <td>{{ $bookAssign->book_id }}</td>
            <td>{{ $bookAssign->user_id }}</td>
            <td>{{ $bookAssign->from_date }}</td>
            <td>{{ $bookAssign->to_date }}</td>
            <td>{{ $bookAssign->submission_date }}</td>
            <td>{{ $bookAssign->status }}</td>
            <td>{{ $bookAssign->transaction }}</td>
                <td width="120">
                    {!! Form::open(['route' => ['bookAssigns.destroy', $bookAssign->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('bookAssigns.show', [$bookAssign->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-eye"></i>
                        </a>
                        <a href="{{ route('bookAssigns.edit', [$bookAssign->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-edit"></i>
                        </a>
                        {!! Form::button('<i class="far fa-trash-alt"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
