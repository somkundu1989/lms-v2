<!-- Book Id Field -->
<div class="col-sm-12">
    {!! Form::label('book_id', 'Book Id:') !!}
    <p>{{ $bookAssign->book_id }}</p>
</div>

<!-- User Id Field -->
<div class="col-sm-12">
    {!! Form::label('user_id', 'User Id:') !!}
    <p>{{ $bookAssign->user_id }}</p>
</div>

<!-- From Date Field -->
<div class="col-sm-12">
    {!! Form::label('from_date', 'From Date:') !!}
    <p>{{ $bookAssign->from_date }}</p>
</div>

<!-- To Date Field -->
<div class="col-sm-12">
    {!! Form::label('to_date', 'To Date:') !!}
    <p>{{ $bookAssign->to_date }}</p>
</div>

<!-- Submission Date Field -->
<div class="col-sm-12">
    {!! Form::label('submission_date', 'Submission Date:') !!}
    <p>{{ $bookAssign->submission_date }}</p>
</div>

<!-- Status Field -->
<div class="col-sm-12">
    {!! Form::label('status', 'Status:') !!}
    <p>{{ $bookAssign->status }}</p>
</div>

<!-- Transaction Field -->
<div class="col-sm-12">
    {!! Form::label('transaction', 'Transaction:') !!}
    <p>{{ $bookAssign->transaction }}</p>
</div>

