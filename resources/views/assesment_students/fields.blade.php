<input type="hidden" name="institute_id" value="{{ institute('id') }}">
{{--
<!-- Institute Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('institute_id', 'Institute Id:') !!}
    {!! Form::number('institute_id', null, ['class' => 'form-control']) !!}
</div>
--}}

<!-- Academic Year Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('academic_year_id', 'Academic Year Id:') !!}
    {!! Form::number('academic_year_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Course Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('course_id', 'Course Id:') !!}
    {!! Form::number('course_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Batch Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('batch_id', 'Batch Id:') !!}
    {!! Form::number('batch_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Subject Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('subject_id', 'Subject Id:') !!}
    {!! Form::number('subject_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Assesment Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('assesment_id', 'Assesment Id:') !!}
    {!! Form::number('assesment_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Student Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('student_id', 'Student Id:') !!}
    {!! Form::number('student_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Answer Stock Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('answer_stock', 'Answer Stock:') !!}
    {!! Form::textarea('answer_stock', null, ['class' => 'form-control','rows' => 2]) !!}
</div>

<!-- Marks Obtained Field -->
<div class="form-group col-sm-6">
    <div class="form-check">
        {!! Form::hidden('marks_obtained', 0, ['class' => 'form-check-input']) !!}
        {!! Form::checkbox('marks_obtained', '1', null, ['class' => 'form-check-input']) !!}
        {!! Form::label('marks_obtained', 'Marks Obtained', ['class' => 'form-check-label']) !!}
    </div>
</div>


<!-- File Pdf Result Field -->
<div class="form-group col-sm-6">
    {!! Form::label('file_pdf_result', 'File Pdf Result:') !!}
    {!! Form::text('file_pdf_result', null, ['class' => 'form-control','maxlength' => 255,'maxlength' => 255]) !!}
</div>