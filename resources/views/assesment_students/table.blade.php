<div class="table-responsive">
    <table class="table" id="assesmentStudents-table">
        <thead>
        <tr>
        <th>Academic Year Id</th>
        <th>Course Id</th>
        <th>Batch Id</th>
        <th>Subject Id</th>
        <th>Assesment Id</th>
        <th>Student Id</th>
        <th>Answer Stock</th>
        <th>Marks Obtained</th>
        <th>File Pdf Result</th>
            <th colspan="3">Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($assesmentStudents as $assesmentStudent)
            <tr>
            <td>{{ $assesmentStudent->academic_year_id }}</td>
            <td>{{ $assesmentStudent->course_id }}</td>
            <td>{{ $assesmentStudent->batch_id }}</td>
            <td>{{ $assesmentStudent->subject_id }}</td>
            <td>{{ $assesmentStudent->assesment_id }}</td>
            <td>{{ $assesmentStudent->student_id }}</td>
            <td>{{ $assesmentStudent->answer_stock }}</td>
            <td>{{ $assesmentStudent->marks_obtained }}</td>
            <td>{{ $assesmentStudent->file_pdf_result }}</td>
                <td width="120">
                    {!! Form::open(['route' => ['assesmentStudents.destroy', $assesmentStudent->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('assesmentStudents.show', [$assesmentStudent->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-eye"></i>
                        </a>
                        <a href="{{ route('assesmentStudents.edit', [$assesmentStudent->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-edit"></i>
                        </a>
                        {!! Form::button('<i class="far fa-trash-alt"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
