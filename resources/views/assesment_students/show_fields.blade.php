<!-- Institute Id Field -->
<div class="col-sm-12">
    {!! Form::label('institute_id', 'Institute Id:') !!}
    <p>{{ $assesmentStudent->institute_id }}</p>
</div>

<!-- Academic Year Id Field -->
<div class="col-sm-12">
    {!! Form::label('academic_year_id', 'Academic Year Id:') !!}
    <p>{{ $assesmentStudent->academic_year_id }}</p>
</div>

<!-- Course Id Field -->
<div class="col-sm-12">
    {!! Form::label('course_id', 'Course Id:') !!}
    <p>{{ $assesmentStudent->course_id }}</p>
</div>

<!-- Batch Id Field -->
<div class="col-sm-12">
    {!! Form::label('batch_id', 'Batch Id:') !!}
    <p>{{ $assesmentStudent->batch_id }}</p>
</div>

<!-- Subject Id Field -->
<div class="col-sm-12">
    {!! Form::label('subject_id', 'Subject Id:') !!}
    <p>{{ $assesmentStudent->subject_id }}</p>
</div>

<!-- Assesment Id Field -->
<div class="col-sm-12">
    {!! Form::label('assesment_id', 'Assesment Id:') !!}
    <p>{{ $assesmentStudent->assesment_id }}</p>
</div>

<!-- Student Id Field -->
<div class="col-sm-12">
    {!! Form::label('student_id', 'Student Id:') !!}
    <p>{{ $assesmentStudent->student_id }}</p>
</div>

<!-- Answer Stock Field -->
<div class="col-sm-12">
    {!! Form::label('answer_stock', 'Answer Stock:') !!}
    <p>{{ $assesmentStudent->answer_stock }}</p>
</div>

<!-- Marks Obtained Field -->
<div class="col-sm-12">
    {!! Form::label('marks_obtained', 'Marks Obtained:') !!}
    <p>{{ $assesmentStudent->marks_obtained }}</p>
</div>

<!-- File Pdf Result Field -->
<div class="col-sm-12">
    {!! Form::label('file_pdf_result', 'File Pdf Result:') !!}
    <p>{{ $assesmentStudent->file_pdf_result }}</p>
</div>

