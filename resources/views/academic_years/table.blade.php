<div class="table-responsive">
    <table class="table" id="academicYears-table">
        <thead>
        <tr>
        <th>Title</th>
        <th>Start Date</th>
        <th>End Date</th>
            <th colspan="3">Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($academicYears as $academicYear)
            <tr>
            <td>{{ $academicYear->title }}</td>
            <td>{{ $academicYear->start_date }}</td>
            <td>{{ $academicYear->end_date }}</td>
                <td width="120">
                    {!! Form::open(['route' => ['academicYears.destroy', $academicYear->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('academicYears.show', [$academicYear->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-eye"></i>
                        </a>
                        <a href="{{ route('academicYears.edit', [$academicYear->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-edit"></i>
                        </a>
                        {!! Form::button('<i class="far fa-trash-alt"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
