<!-- Institute Id Field -->
<div class="col-sm-12">
    {!! Form::label('institute_id', 'Institute Id:') !!}
    <p>{{ $subject->institute_id }}</p>
</div>

<!-- Academic Year Id Field -->
<div class="col-sm-12">
    {!! Form::label('academic_year_id', 'Academic Year Id:') !!}
    <p>{{ $subject->academic_year_id }}</p>
</div>

<!-- Course Id Field -->
<div class="col-sm-12">
    {!! Form::label('course_id', 'Course Id:') !!}
    <p>{{ $subject->course_id }}</p>
</div>

<!-- Title Field -->
<div class="col-sm-12">
    {!! Form::label('title', 'Title:') !!}
    <p>{{ $subject->title }}</p>
</div>

<!-- Syllabus Field -->
<div class="col-sm-12">
    {!! Form::label('syllabus', 'Syllabus:') !!}
    <p>{{ $subject->syllabus }}</p>
</div>

<!-- Reference Book Field -->
<div class="col-sm-12">
    {!! Form::label('reference_book', 'Reference Book:') !!}
    <p>{{ $subject->reference_book }}</p>
</div>

