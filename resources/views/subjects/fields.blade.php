<input type="hidden" name="institute_id" value="{{ institute('id') }}">
{{--
<!-- Institute Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('institute_id', 'Institute Id:') !!}
    {!! Form::number('institute_id', null, ['class' => 'form-control']) !!}
</div>
--}}

<!-- Academic Year Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('academic_year_id', 'Academic Year Id:') !!}
    {!! Form::select('academic_year_id', academicYears(), null, ['class' => 'form-control']) !!}
</div>

    {{--<select class="form-control" multiple name="courses[]">
        @foreach(courses() as $obj)
            <option value="{{$obj->id}}" {{ ((!empty($subject))?((((int)stripos($subject->courses, '"'.$obj->id.'"'))>0) ? 'selected' : ''): '') }} > {{$obj->title}}</option>
        @endforeach
    </select>--}}
<!-- Course Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('courses', 'Courses:') !!}
    {!! Form::select('courses[]', courses(), ((!empty($subject))? json_decode($subject->courses): null), ['class' => 'form-control','multiple'=>true]) !!}
</div>

<!-- Title Field -->
<div class="form-group col-sm-6">
    {!! Form::label('title', 'Title:') !!}
    {!! Form::text('title', null, ['class' => 'form-control','maxlength' => 255,'maxlength' => 255]) !!}
</div>

<!-- Syllabus Field -->
<div class="form-group col-sm-6">
    {!! Form::label('syllabus', 'Syllabus:') !!}
    {!! Form::textarea('syllabus', null, ['class' => 'form-control','rows' => 2]) !!}
</div>

<!-- Reference Book Field -->
<div class="form-group col-sm-6">
    {!! Form::label('reference_book', 'Reference Book:') !!}
    {!! Form::textarea('reference_book', null, ['class' => 'form-control','rows' => 2]) !!}
</div>