<!-- User Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('user_id', 'User Id:') !!}
    {!! Form::number('user_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Title Field -->
<div class="form-group col-sm-6">
    {!! Form::label('title', 'Title:') !!}
    {!! Form::text('title', null, ['class' => 'form-control','maxlength' => 255,'maxlength' => 255]) !!}
</div>

<!-- Desciption Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('desciption', 'Desciption:') !!}
    {!! Form::textarea('desciption', null, ['class' => 'form-control','rows' => 2]) !!}
</div>

<!-- Educations Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('educations', 'Educations:') !!}
    {!! Form::textarea('educations', null, ['class' => 'form-control','rows' => 2]) !!}
</div>

<!-- Experiences Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('experiences', 'Experiences:') !!}
    {!! Form::textarea('experiences', null, ['class' => 'form-control','rows' => 2]) !!}
</div>

<!-- Skills Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('skills', 'Skills:') !!}
    {!! Form::textarea('skills', null, ['class' => 'form-control','rows' => 2]) !!}
</div>

<!-- Languages Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('languages', 'Languages:') !!}
    {!! Form::textarea('languages', null, ['class' => 'form-control','rows' => 2]) !!}
</div>

<!-- Testimonials Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('testimonials', 'Testimonials:') !!}
    {!! Form::textarea('testimonials', null, ['class' => 'form-control','rows' => 2]) !!}
</div>