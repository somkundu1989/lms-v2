<!-- User Id Field -->
<div class="col-sm-12">
    {!! Form::label('user_id', 'User Id:') !!}
    <p>{{ $profile->user_id }}</p>
</div>

<!-- Title Field -->
<div class="col-sm-12">
    {!! Form::label('title', 'Title:') !!}
    <p>{{ $profile->title }}</p>
</div>

<!-- Desciption Field -->
<div class="col-sm-12">
    {!! Form::label('desciption', 'Desciption:') !!}
    <p>{{ $profile->desciption }}</p>
</div>

<!-- Educations Field -->
<div class="col-sm-12">
    {!! Form::label('educations', 'Educations:') !!}
    <p>{{ $profile->educations }}</p>
</div>

<!-- Experiences Field -->
<div class="col-sm-12">
    {!! Form::label('experiences', 'Experiences:') !!}
    <p>{{ $profile->experiences }}</p>
</div>

<!-- Skills Field -->
<div class="col-sm-12">
    {!! Form::label('skills', 'Skills:') !!}
    <p>{{ $profile->skills }}</p>
</div>

<!-- Languages Field -->
<div class="col-sm-12">
    {!! Form::label('languages', 'Languages:') !!}
    <p>{{ $profile->languages }}</p>
</div>

<!-- Testimonials Field -->
<div class="col-sm-12">
    {!! Form::label('testimonials', 'Testimonials:') !!}
    <p>{{ $profile->testimonials }}</p>
</div>

