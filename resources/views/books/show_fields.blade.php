<!-- Genre Id Field -->
<div class="col-sm-12">
    {!! Form::label('genre_id', 'Genre Id:') !!}
    <p>{{ $book->genre_id }}</p>
</div>

<!-- Author Id Field -->
<div class="col-sm-12">
    {!! Form::label('author_id', 'Author Id:') !!}
    <p>{{ $book->author_id }}</p>
</div>

<!-- Publisher Id Field -->
<div class="col-sm-12">
    {!! Form::label('publisher_id', 'Publisher Id:') !!}
    <p>{{ $book->publisher_id }}</p>
</div>

<!-- Course Id Field -->
<div class="col-sm-12">
    {!! Form::label('course_id', 'Course Id:') !!}
    <p>{{ $book->course_id }}</p>
</div>

<!-- Subject Id Field -->
<div class="col-sm-12">
    {!! Form::label('subject_id', 'Subject Id:') !!}
    <p>{{ $book->subject_id }}</p>
</div>

<!-- Title Field -->
<div class="col-sm-12">
    {!! Form::label('title', 'Title:') !!}
    <p>{{ $book->title }}</p>
</div>

<!-- Description Field -->
<div class="col-sm-12">
    {!! Form::label('description', 'Description:') !!}
    <p>{{ $book->description }}</p>
</div>

<!-- File Field -->
<div class="col-sm-12">
    {!! Form::label('file', 'File:') !!}
    <p>{{ $book->file }}</p>
</div>

