<div class="table-responsive">
    <table class="table" id="books-table">
        <thead>
        <tr>
            <th>Genre Id</th>
        <th>Author Id</th>
        <th>Publisher Id</th>
        <th>Course Id</th>
        <th>Subject Id</th>
        <th>Title</th>
        <th>Description</th>
        <th>File</th>
            <th colspan="3">Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($books as $book)
            <tr>
                <td>{{ $book->genre_id }}</td>
            <td>{{ $book->author_id }}</td>
            <td>{{ $book->publisher_id }}</td>
            <td>{{ $book->course_id }}</td>
            <td>{{ $book->subject_id }}</td>
            <td>{{ $book->title }}</td>
            <td>{{ $book->description }}</td>
            <td>{{ $book->file }}</td>
                <td width="120">
                    {!! Form::open(['route' => ['books.destroy', $book->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('books.show', [$book->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-eye"></i>
                        </a>
                        <a href="{{ route('books.edit', [$book->id]) }}"
                           class='btn btn-default btn-xs'>
                            <i class="far fa-edit"></i>
                        </a>
                        {!! Form::button('<i class="far fa-trash-alt"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
